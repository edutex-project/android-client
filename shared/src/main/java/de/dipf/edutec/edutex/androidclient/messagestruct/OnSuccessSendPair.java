package de.dipf.edutec.edutex.androidclient.messagestruct;

import org.json.JSONException;
import org.json.JSONObject;

public class OnSuccessSendPair {

    boolean sendSuccessfull;
    boolean isSurveyBool;
    String uuid;

    public OnSuccessSendPair(JSONObject message, boolean sendSuccessfull){

        try{
            String isSurvey = message.getString("isSurvey");
            if(isSurvey.equals("True")){

                this.isSurveyBool = true;
                this.uuid = message.getString("survey_id");

            } else {

                this.isSurveyBool = true;
                this.uuid = message.getString("id");

            }

        } catch (JSONException e) {
            //e.printStackTrace();
        }

        this.sendSuccessfull = sendSuccessfull;
    }

    public boolean getSuccess(){return this.sendSuccessfull;}
    public String getUuid(){return this.uuid;}
    public Boolean isSurvey(){return this.isSurveyBool;}


}
