package de.dipf.edutec.edutex.androidclient.sharedUtils;

public class MessageLayerStatics {

    // Backend Messages
    public static final String TOSMARTWATCH_QUESTIONNAIRE = "/tosmartwatch/survey";
    public static final String TOSMARTWATCH_PRE_POST_QUESTIONNAIRE = "/tosmartwatch/survey_pre";
    public static final String TOSMARTWATCH_MICRO_SURVEY = "/tosmartwatch/micro_survey";
    public static final String TOSMARTWATCH_NOTIFICATION = "/tosmartwatch/notification";
    public static final String TOSMARTWATCH_SENSOR_REQUEST = "/tosmartwatch/backend_request";
    public static final String TOSMARTWATCH_ERROR_NOTIFICATION = "/tosmartwatch/error_notification";

    // Control Messages
    public static final String TOSMARTWATCH_REQUEST_RECOVERY = "/tosmartwatch/recovery_question";
    public static final String TOSMARTWATCH_RESPONSE_RECOVERY = "/tosmartwatch/response_recovery";
    public static final String TOSMARTWATCH_CONTROL_FLOW = "/tosmartwatch/control_flow";
    public static final String TOSMARTWATCH_RESPONSE_SESSION_STATE = "/tosmartwatch/response_session_state";
    public static final String TOSMARTWATCH_RESPONSE_ACRA_CREDENTIALS = "/tosmartwatch/response_acra_cred";


    public static final String TOHANDHELD_RESPONSE_SENSOR_REQUEST = "/tohandheld/response_sensor_request";
    public static final String TOHANDHELD_RESPONSE_NOTIFICATIONS = "/tohandheld/response_notification";
    public static final String TOHANDHELD_RESPONSE_QUESTIONNAIRE = "/tohandheld/response_questionnaire";
    public static final String TOHANDHELD_RESPONSE_MICRO_QUESTIONNAIRE = "/tohandheld/response_micro_questionnaire";


    public static final String TOHANDHELD_REQUEST_SESSION_STATE = "/tohandheld/request_session_state";
    public static final String TOHANDHELD_ESM_SERVICE_STOP = "/tohandheld/esm_service_stop";
    public static final String TOHANDHELD_REQUEST_ACRA_CREDENTIALS = "/tohandheld/acra_credentials";


    public static final String TOHANDHELD_REQUEST_SESSIONSTATE="/tohandheld/request_session_state";
    public static final String TOHANDHELD_START_SESSION="/tohandheld/esm_service_start";
    public static final String TOHANDHELD_STOP_SESSION="/tohandheld/esm_service_stop";
    public static final String TOHANDHELD_RESPONSE_RECOVERY= "/tohandheld/response_recovery";

    public static final String TOHANDHELD_SMARTWATCH_CRASHED= "/tohandheld/smartwatch_crashed";


    public static final String TOHANDHELD_PING = "/tohandheld/ping";
    public static final String TOSMARTWATCH_PING = "/tosmartwatch/ping";



}
