package de.dipf.edutec.edutex.androidclient.conf;

import android.app.NotificationManager;
import android.content.Context;

import de.dipf.edutec.edutex.androidclient.activities.MainActivity;
import de.dipf.edutec.edutex.androidclient.foreground.ForegroundNotificationCreator;
import lombok.Getter;

@Getter
public class ApplicationContext {
    private static final String CHANNEL_ID = "ForegroundServiceChannel";
    private static final String NOTIFICATION_CONTENT_TITLE = "App is still running";
    private static final String NOTIFICATION_CONTENT_TEXT = "Tap to go back to the app.";

    private final ForegroundNotificationCreator foregroundNotificationCreator;

    public ApplicationContext(Context ctx, NotificationManager notificationManager) {

        this.foregroundNotificationCreator = new ForegroundNotificationCreator(
                1,
                CHANNEL_ID,
                ctx,
                MainActivity.class,
                notificationManager,
                NOTIFICATION_CONTENT_TITLE,
                NOTIFICATION_CONTENT_TEXT,
                de.dipf.edutec.edutex.androidclient.shared.R.drawable.ic_notifications_black_24dp);
    }

    public ForegroundNotificationCreator getForegroundNotificationCreator() {
        return foregroundNotificationCreator;
    }
}
