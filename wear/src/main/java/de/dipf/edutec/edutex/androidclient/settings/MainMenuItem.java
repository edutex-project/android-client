package de.dipf.edutec.edutex.androidclient.settings;

public class MainMenuItem {
    private String text;
    private int image;

    public MainMenuItem(int image, String text) {
        this.image = image;
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public int getImage() {
        return image;
    }
}
