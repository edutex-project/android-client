package de.dipf.edutec.edutex.androidclient.activities;

import static com.google.android.gms.wearable.Wearable.getCapabilityClient;
import static java.lang.StrictMath.abs;
import static java.lang.StrictMath.toIntExact;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.wearable.activity.WearableActivity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.wearable.CapabilityClient;

import java.util.ArrayList;
import java.util.UUID;

import de.dipf.edutec.edutex.androidclient.BuildConfig;
import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.boot.RunAfterBootService;
import de.dipf.edutec.edutex.androidclient.dialogs.PrivacyDialog;
import de.dipf.edutec.edutex.androidclient.messageservice.MessageLayerSender;
import de.dipf.edutec.edutex.androidclient.microsurvey.MicroQuestionnaireSchedulingService;
import de.dipf.edutec.edutex.androidclient.utils.CommunitcationUtils;
import de.dipf.edutec.edutex.androidclient.utils.NotificationChannelUtils;
import de.dipf.edutec.edutex.androidclient.utils.PermissionUtils;
import de.dipf.edutec.edutex.androidclient.utils.ServiceRunningUtil;
import de.dipf.edutec.edutex.androidclient.utils.SessionPreferences;
import pl.droidsonroids.gif.GifImageView;
import de.dipf.edutec.edutex.androidclient.sharedUtils.MessageLayerStatics;

public class MainActivity extends WearableActivity {

    private static final String TAG = "MainActivity";

    private static Context mContext;
    public ImageButton startSession, settingsButton;
    public GifImageView preloader;
    public TextView remoteHelpText, refreshText;
    private BroadcastReceiver guiBroadcastReceiver;
    private ArrayList<BroadcastReceiver> broadcastReceivers;
    Handler handler;

    public static Context getContext() {
        return mContext;
    }

    @Override
    protected void onDestroy() {
        unregisterAllBroadcastReceivers();

        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(this);
        manager.unregisterReceiver(guiBroadcastReceiver);

        handler.removeCallbacksAndMessages(null);

        Log.d(TAG, "Unregistered Main UI Broadcastreceiver");
        super.onDestroy();
    }



    @Override
    public void onResume() {

        super.onResume();
        Log.d(TAG, "onResume is called");

        // Registering Broadcast Receiver for UI interventions
        guiBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String command = intent.getStringExtra("command");
                switch (command) {
                    case "enable_start_button":
                        startSession.setVisibility(View.VISIBLE);
                        preloader.setVisibility(View.GONE);
                        remoteHelpText.setVisibility(View.GONE);
                        refreshText.setVisibility(View.GONE);
                        if (!Boolean.valueOf(getString(R.string.DEBUG_MODE))) {
                            setGestureDetector();
                        }
                        break;
                    case "disable_start_button":
                        startSession.setVisibility(View.GONE);
                        preloader.setVisibility(View.VISIBLE);
                        remoteHelpText.setVisibility(View.GONE);
                        refreshText.setVisibility(View.VISIBLE);
                        mDetector = null;
                        break;
                    case "exclude_smartwatch":
                        startSession.setVisibility(View.GONE);
                        preloader.setVisibility(View.VISIBLE);
                        remoteHelpText.setVisibility(View.GONE);
                        refreshText.setVisibility(View.VISIBLE);
                        break;
                    case "remote_started":
                        startSession.setVisibility(View.GONE);
                        preloader.setVisibility(View.VISIBLE);
                        remoteHelpText.setVisibility(View.VISIBLE);
                        refreshText.setVisibility(View.VISIBLE);
                        break;
                    default:
                        break;
                }
            }
        };

        IntentFilter main_ui_filter = new IntentFilter(getString(R.string.ACTION_MAIN_UI));
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(this);
        manager.registerReceiver(guiBroadcastReceiver, main_ui_filter);
        Log.d(TAG, "Registered Main UI Broadcastreceiver");

        SessionPreferences sessionPreferences = new SessionPreferences(getApplicationContext());
        sessionPreferences.setSessionRunning(false);

        MessageLayerSender messageLayerSender = new MessageLayerSender(getApplicationContext());
        messageLayerSender.sendAck(CommunitcationUtils.TOHANDHELD_REQUEST_SESSION_STATE, "{}");

        if(! sessionPreferences.isAcraSet() ){
            Log.d(TAG,"ACRA User is not set. Requesting..");
            messageLayerSender = new MessageLayerSender(getContext());
            messageLayerSender.sendAck(MessageLayerStatics.TOHANDHELD_REQUEST_ACRA_CREDENTIALS,"{}");
        } else {
            Log.d(TAG, "ACRA User is set.");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        handler = new Handler();
        broadcastReceivers = new ArrayList<>();

        mContext = getApplicationContext();
        setContentView(R.layout.activity_main);
        setAmbientEnabled();
        findGuiElements();

        NotificationChannelUtils.create_notification_channel_if_not_exist(getApplicationContext());
        SessionPreferences sessionPreferences = new SessionPreferences(getApplicationContext());
        sessionPreferences.createInternalSessionVariables();

        if(!sessionPreferences.getPrivayNotificationSended()){
            PrivacyDialog privacyDialog = new PrivacyDialog(this, this);
            privacyDialog.showDialog();
        }

        // Send Signal to Handheld that Smartwatch is reachable
        getCapabilityClient(getApplicationContext())
                .getCapability("bluetooth-status-wear", CapabilityClient.FILTER_REACHABLE);


        startSession.setOnClickListener(v -> {

            startSession.setVisibility(View.GONE);
            preloader.setVisibility(View.VISIBLE);
            Toast.makeText(getApplicationContext(), "Sending Start..", Toast.LENGTH_SHORT).show();

            MessageLayerSender messageLayerSender = new MessageLayerSender(getApplicationContext());
            String deliveryUUID = messageLayerSender.sendStartSession();

            Runnable sessionStartDeliverTimer = new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), "Missing Bluetooth connection", Toast.LENGTH_LONG).show();
                    unregisterAllBroadcastReceivers();
                    refreshText.setVisibility(View.VISIBLE);
                }
            };

            BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {

                    Log.d(TAG,"Inside broadcastreceiver");
                    Log.d(TAG, intent.getStringExtra("ping"));
                    Log.d(TAG, deliveryUUID);

                    if(intent.getStringExtra("ping").equals(deliveryUUID)){

                        Log.d(TAG,"Inside broadcastreceiver ping");
                        handler.removeCallbacks(sessionStartDeliverTimer);
                        handler.removeCallbacksAndMessages(null);

                        onDestroy();
                        startActivity(new Intent(getApplicationContext(), SessionActivity.class));

                    }
                }
            };
            registerBroadcastReceiverForDelivery(broadcastReceiver);
            handler.postDelayed(sessionStartDeliverTimer,15000);

        });

        settingsButton = findViewById(R.id.ib_main_settings);
        settingsButton.setOnClickListener(v -> {
            startActivity(new Intent(getContext(), SettingsActivity.class));
        });

        preloader.setOnClickListener(v -> {

            BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mBluetoothAdapter == null) {
                Toast.makeText(getApplicationContext(), "Your device does not support bluetooth",
                        Toast.LENGTH_SHORT).show();
            } else if (!mBluetoothAdapter.isEnabled()) {
                Toast.makeText(getApplicationContext(), "Please enable bluetooth",
                        Toast.LENGTH_SHORT).show();
            } else{
                MessageLayerSender messageLayerSender = new MessageLayerSender(getContext());
                messageLayerSender.sendAck(CommunitcationUtils.TOHANDHELD_REQUEST_SESSION_STATE, "");
                Toast.makeText(getApplicationContext(),"Pending..",Toast.LENGTH_SHORT).show();
            }


        });

        if(!ServiceRunningUtil.isMyServiceRunning(MicroQuestionnaireSchedulingService.class, getApplicationContext())){
            getApplicationContext().startService(new Intent(getApplicationContext(),RunAfterBootService.class));
        }


    }

    public void findGuiElements() {
        startSession = findViewById(R.id.ib_main_start_session);
        settingsButton = findViewById(R.id.ib_main_settings);
        preloader = findViewById(R.id.iv_progressidalog_gif);
        remoteHelpText = findViewById(R.id.remoteHelpText);
        refreshText = findViewById(R.id.refresh_text);
        TextView versionName = findViewById(R.id.main_versionName);
        versionName.setText("Version: " + BuildConfig.VERSION_NAME);
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        PermissionUtils permissionUtils = new PermissionUtils(this);
        permissionUtils.displayRequests();


    }

    private GestureDetector mDetector;
    private long lastGestureEvent;

    private void setGestureDetector() {

        mDetector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener() {


            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
                                    float distanceY) {

                if (System.currentTimeMillis() - lastGestureEvent > 500L) {
                    lastGestureEvent = System.currentTimeMillis();
                    if (abs(distanceX) > 5) {

                        if (distanceY < 0) {
                            settingsButton.setVisibility(View.VISIBLE);
                            startSession.setVisibility(View.GONE);
                        }

                        if (distanceY > 0) {
                            settingsButton.setVisibility(View.GONE);
                            startSession.setVisibility(View.VISIBLE);
                        }
                    }
                }
                return true;
            }

        });
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (mDetector != null) {
            return mDetector.onTouchEvent(ev) || super.dispatchTouchEvent(ev);
        } else {
            return super.dispatchTouchEvent(ev);
        }
    }


    public void registerBroadcastReceiverForDelivery(BroadcastReceiver receiver){
        IntentFilter ping_filter = new IntentFilter("PING_FILTER");
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(this);
        manager.registerReceiver(receiver, ping_filter);
        broadcastReceivers.add(receiver);


    }

    public void unregisterAllBroadcastReceivers(){
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(this);
        for(BroadcastReceiver receiver: broadcastReceivers){
            try{
                manager.unregisterReceiver(receiver);
            }catch (Exception er){
                Log.e(TAG,"BroadcastReceiver could not be unregistered.");
            }
        }
    }




}
