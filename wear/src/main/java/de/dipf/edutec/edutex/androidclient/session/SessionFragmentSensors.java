package de.dipf.edutec.edutex.androidclient.session;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.sensorservice.SensorDataService;

public class SessionFragmentSensors extends Fragment {

    static String TAG = "SessionsFragmentSensors";

    LocalBroadcastManager localBroadcastManager;
    BroadcastReceiver broadcastReceiver;
    SensorManager sensorManager;
    List<String> sensor_names;
    List<Integer> sensor_types;
    SharedPreferences sensorPreferences;
    int current_displayed_sensor_index;

    TextView title, tvX, tvY, tvZ, tvSingle;
    LinearLayout ll_single_value, ll_triple_value;

    public SessionFragmentSensors() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        current_displayed_sensor_index = -1;
        localBroadcastManager = LocalBroadcastManager.getInstance(getContext());
        //sensorManager = (SensorManager) getContext().getSystemService(Context.SENSOR_SERVICE);
        sensorPreferences = getContext().getSharedPreferences("SensorPreferences", Context.MODE_PRIVATE);
        sensor_names = new ArrayList<>();
        sensor_types = new ArrayList<>();

        /*
        for(int i = 0; i  < SensorDataService.ENABLED_SENSORS.length; i++){
            Sensor tmp = sensorManager.getDefaultSensor(SensorDataService.ENABLED_SENSORS[i]);
            try{
                int selected_sensor = sensorPreferences.getInt(tmp.getName(),1);
                if(selected_sensor == 1){
                    sensor_names.add(tmp.getName());
                    sensor_types.add(tmp.getType());
                }

                Log.d(TAG, "Registering Sensors: " + tmp + " " + String.valueOf(selected_sensor));
            } catch (Exception e){
                Log.d(TAG,"..");
            }

        }      */

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_session_sensors, container, false);
    }

    @Override
    public void onStart(){
        super.onStart();
        findGUIElements();
        switchToNextSensor();
    }



    public boolean switchToNextSensor(){
        Log.d(TAG, "Switch to next is called");

        if(sensor_names.size() > current_displayed_sensor_index + 1 ){
            current_displayed_sensor_index += 1;
            String new_title = sensor_names.get(current_displayed_sensor_index);
            title.setText(new_title);

            if(broadcastReceiver != null){
                localBroadcastManager.unregisterReceiver(broadcastReceiver);
            }


            if(sensor_types.get(current_displayed_sensor_index) == Integer.valueOf(Sensor.TYPE_LIGHT)){

                Log.d(TAG, "Preparing for Sensor Type Light");
                ll_triple_value.setVisibility(View.GONE);
                ll_single_value.setVisibility(View.VISIBLE);
                broadcastReceiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        float[] extras = intent.getFloatArrayExtra("values");
                        tvSingle.setText(String.valueOf(extras[0]));
                    }
                };
            } else if (sensor_types.get(current_displayed_sensor_index) != Integer.valueOf(Sensor.TYPE_LIGHT)){
                Log.d(TAG, "Preparing for Sensor");
                ll_single_value.setVisibility(View.GONE);
                ll_triple_value.setVisibility(View.VISIBLE);
                broadcastReceiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        float[] extras = intent.getFloatArrayExtra("values");
                        tvX.setText(String.valueOf(extras[0]));
                        tvY.setText(String.valueOf(extras[1]));
                        tvZ.setText(String.valueOf(extras[2]));
                    }
                };
            }

            localBroadcastManager.registerReceiver(
                    broadcastReceiver,
                    new IntentFilter(sensor_names.get(current_displayed_sensor_index)));
            return true;
        } else {
            return false;
        }





    }

    public boolean switchToLastSensor(){
        Log.d(TAG, "Switch to last is called");

        if(current_displayed_sensor_index - 1 >= 0 && sensor_names.size() > current_displayed_sensor_index - 1){
            current_displayed_sensor_index -= 1;
            String new_title = sensor_names.get(current_displayed_sensor_index);
            title.setText(new_title);

            if(broadcastReceiver != null){
                localBroadcastManager.unregisterReceiver(broadcastReceiver);
            }

            if(sensor_types.get(current_displayed_sensor_index) == Integer.valueOf(Sensor.TYPE_LIGHT)){
                ll_triple_value.setVisibility(View.GONE);
                ll_single_value.setVisibility(View.VISIBLE);
                broadcastReceiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        float[] extras = intent.getFloatArrayExtra("values");
                        tvSingle.setText(String.valueOf(extras[0]));
                    }
                };
            } else if (sensor_types.get(current_displayed_sensor_index) != Integer.valueOf(Sensor.TYPE_LIGHT)){
                ll_single_value.setVisibility(View.GONE);
                ll_triple_value.setVisibility(View.VISIBLE);
                broadcastReceiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        float[] extras = intent.getFloatArrayExtra("values");
                        tvX.setText(String.valueOf(extras[0]));
                        tvY.setText(String.valueOf(extras[1]));
                        tvZ.setText(String.valueOf(extras[2]));
                    }
                };
            }

            localBroadcastManager.registerReceiver(
                    broadcastReceiver,
                    new IntentFilter(sensor_names.get(current_displayed_sensor_index)));

            return true;
        } else {
            return false;
        }
    }

    private void findGUIElements(){
        title = getActivity().findViewById(R.id.tv_frag_sensors_title);
        tvSingle = getActivity().findViewById(R.id.tv_session_sensors_single);
        tvX = getActivity().findViewById(R.id.tv_session_sensors_x);
        tvY = getActivity().findViewById(R.id.tv_session_sensors_y);
        tvZ = getActivity().findViewById(R.id.tv_session_sensors_z);
        ll_single_value = getActivity().findViewById(R.id.ll_single_value);
        ll_triple_value = getActivity().findViewById(R.id.ll_triple_value);
    }

}