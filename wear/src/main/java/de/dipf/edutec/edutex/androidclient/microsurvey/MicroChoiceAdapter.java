package de.dipf.edutec.edutex.androidclient.microsurvey;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import de.dipf.edutec.edutex.androidclient.R;
public class MicroChoiceAdapter extends RecyclerView.Adapter<MicroChoiceAdapter.ViewHolder>{
    private static String TAG = "MicroChoiceAdapter";
    private Context mContext;
    private List<String> mAnswerChoices;
    private LayoutInflater mInflater;
    private MicroItemClickListener mItemClickListener;

    public MicroChoiceAdapter(Context context, List<String> choices){
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mAnswerChoices = choices;
        Log.d(TAG, "MicroChoiceAdapter created " + mAnswerChoices.get(0));
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d(TAG, "Created View");
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_micro_survey_text, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.d(TAG, "Setted.");
        String choice = mAnswerChoices.get(position);
        holder.mChoiceView.setText(choice);


    }



    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView mChoiceView;

        public ViewHolder(@NonNull View itemView){
            super(itemView);
            Log.d(TAG, "Viewholder created");

            mChoiceView = itemView.findViewById(R.id.tv_choice);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(mItemClickListener != null) mItemClickListener.onItemClick(itemView, getAdapterPosition());
        }


    }


    @Override
    public int getItemCount() {
        return mAnswerChoices.size();
    }

    public void setClickListener(MicroItemClickListener itemClickListener){
        this.mItemClickListener = itemClickListener;
    }


    public interface MicroItemClickListener {
        void onItemClick(View view, int position);
    }


}
