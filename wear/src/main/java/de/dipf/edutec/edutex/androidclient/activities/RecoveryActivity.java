package de.dipf.edutec.edutex.androidclient.activities;

import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.messageservice.MessageLayerSender;

public class RecoveryActivity extends WearableActivity {

    TextView textView;
    Button bt_recover, bt_dont_recover;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recovery);
        findGuiElements();
    }

    private void sendResponse(Boolean recover){
        MessageLayerSender messageLayerSender = new MessageLayerSender(this);
        messageLayerSender.sendAck(getString(R.string.PATH_TO_HANDHELD_RESPONSE_RECOVERY),String.valueOf(recover));
    }

    private void requestState(){
        MessageLayerSender messageLayerSender = new MessageLayerSender(this);
        messageLayerSender.sendAck(getString(R.string.PATH_TO_HANDHELD_REQUEST_SESSION_STATE),"");
    }

    private void findGuiElements(){
        textView = findViewById(R.id.tv_recovery_activity);

        bt_recover = findViewById(R.id.bt_do_recover);
        bt_recover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendResponse(true);
                finish();
            }
        });

        bt_dont_recover = findViewById(R.id.bt_dont_recover);
        bt_dont_recover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendResponse(false);
                requestState();
                finish();
            }
        });

    }
}