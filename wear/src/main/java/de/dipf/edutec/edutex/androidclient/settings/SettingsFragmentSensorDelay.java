package de.dipf.edutec.edutex.androidclient.settings;

import android.content.SharedPreferences;
import android.hardware.SensorManager;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceManager;
import androidx.wear.widget.WearableLinearLayoutManager;
import androidx.wear.widget.WearableRecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.activities.SettingsActivity;

public class SettingsFragmentSensorDelay extends Fragment {

    private static String TAG = "SettingsFragmentSensorDelay";
    WearableRecyclerView wearableRecyclerView;
    public SettingsFragmentSensorDelay() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_settings_sensor_delay, container, false);
    }

    public static final int[] SENSOR_DELAY_VALUE = new int[]{
            SensorManager.SENSOR_DELAY_FASTEST,
            SensorManager.SENSOR_DELAY_GAME,
            SensorManager.SENSOR_DELAY_NORMAL,
            SensorManager.SENSOR_DELAY_UI,
    };

    public static final String[] SENSOR_DELAY_NAME = new String[]{
            "DELAY FASTEST",
            "DELAY GAME",
            "DELAY NORMAL",
            "DELAY UI",
    };

    @Override
    public void onStart(){
        super.onStart();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = sharedPreferences.edit();

        wearableRecyclerView = getActivity().findViewById(R.id.rv_settings_sensor_delay);
        wearableRecyclerView.setHasFixedSize(true);
        wearableRecyclerView.setEdgeItemsCenteringEnabled(true);
        wearableRecyclerView.setLayoutManager(new WearableLinearLayoutManager(getContext()));

        setWearableRecyclerView();

    }

    public void setWearableRecyclerView(){
        ArrayList<MainMenuItem> menuItems = new ArrayList<>();
        for(int i = 0; i < SENSOR_DELAY_VALUE.length; i++){
            menuItems.add(new MainMenuItem(R.drawable.ic_baseline_radio_button_unchecked_24, SENSOR_DELAY_NAME[i]));
        }


        // Return to the overview Settings
        menuItems.add(new MainMenuItem(R.drawable.ic_baseline_arrow_back_24,"Back to Settings"));
        int key_back_to_settings = menuItems.size() - 1;
        Log.d(TAG, "key_back_to_settings value: " + String.valueOf(key_back_to_settings));
        wearableRecyclerView.setAdapter(new SensorDelayMenuAdapter(getContext(), menuItems, new SensorDelayMenuAdapter.AdapterCallback() {
            @Override
            public void onItemClicked(Integer menuPosition) {

                Log.d(TAG, "Pressed menuPosition " + String.valueOf(menuPosition));

                if(menuPosition == key_back_to_settings){
                    ((SettingsActivity)getActivity()).changeFragment("SettingsFragmentMain");
                } else {
                    setWearableRecyclerView();
                }

            }
        }));

    }
}