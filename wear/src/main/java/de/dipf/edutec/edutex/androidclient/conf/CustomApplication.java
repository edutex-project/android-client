package de.dipf.edutec.edutex.androidclient.conf;

import android.app.Application;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;

import de.dipf.edutec.edutex.androidclient.messageservice.MessageLayerSender;
import de.dipf.edutec.edutex.androidclient.messageservice.VibrationService;
import lombok.Getter;

@Getter
public class CustomApplication extends Application {
    private ApplicationContext context;

    private static Thread.UncaughtExceptionHandler mDefaultUncaughtExceptionHandler;

    private Thread.UncaughtExceptionHandler mCaughtExceptionHandler = new Thread.UncaughtExceptionHandler() {
        @Override
        public void uncaughtException(Thread thread, Throwable ex) {
            // Vibrate
            Intent intent = new Intent(getApplicationContext(), VibrationService.class);
            getApplicationContext().startForegroundService(intent);

            // Notify Handheld
            MessageLayerSender messageLayerSender = new MessageLayerSender(getApplicationContext());
            messageLayerSender.sendCrashNotification();

            mDefaultUncaughtExceptionHandler.uncaughtException(thread, ex);
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        this.context = new ApplicationContext(
                getApplicationContext(),
                getSystemService(NotificationManager.class));

        // Second, cache a reference to default uncaught exception handler
        mDefaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
        // Third, set custom UncaughtExceptionHandler
        Thread.setDefaultUncaughtExceptionHandler(mCaughtExceptionHandler);
    }


    public ApplicationContext getContext() {
        return context;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

      /*
      CoreConfigurationBuilder builder = new CoreConfigurationBuilder(this);
      //core configuration:
      builder
              .withBuildConfigClass(BuildConfig.class)
              .withReportFormat(StringFormat.JSON);

      builder.getPluginConfigurationBuilder(HttpSenderConfigurationBuilder.class)
              .withBasicAuthLogin(getResources().getString(R.string.acra_user))
              .withBasicAuthPassword(getResources().getString(R.string.acra_password))
              .withUri(getResources().getString(R.string.acra_url))
              .withSocketTimeout(20000)
              .withConnectionTimeout(20000)
              .withEnabled(true);


      builder.getPluginConfigurationBuilder(LimiterConfigurationBuilder.class)
              .withEnabled(true)
              .withDeleteReportsOnAppUpdate(true)
              .withFailedReportLimit(5)
              .withStacktraceLimit(5);

      //each plugin you chose above can be configured with its builder like this:
      builder.getPluginConfigurationBuilder(NotificationConfigurationBuilder.class)
              .withEnabled(true)
              .withResTitle(R.string.acra_notification_title)
              .withResText(R.string.acra_notification_text)
              .withResChannelName(R.string.acra_notification_channel_name)
              .withResChannelDescription(R.string.acra_notification_channel_description)
              .withResChannelImportance(NotificationManager.IMPORTANCE_HIGH)
              .withResTickerText(R.string.acra_notification_ticker)
              .withResIcon(R.drawable.ic_notifications_black_24dp)
              .withResSendButtonText(R.string.acra_notification_submit)
              .withResDiscardButtonText(R.string.acra_notification_discard)
              .withResSendWithCommentButtonText(R.string.acra_notification_send_with_comment)
              .withResSendWithCommentButtonIcon(R.drawable.icon_reply)
              .withResCommentPrompt(R.string.acra_notification_comment_hint)
              .withSendOnClick(false);

      ACRA.init(this, builder);


      */
    }

}
