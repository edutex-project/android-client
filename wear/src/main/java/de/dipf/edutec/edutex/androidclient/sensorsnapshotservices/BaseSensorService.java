package de.dipf.edutec.edutex.androidclient.sensorsnapshotservices;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;

import androidx.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;

import de.dipf.edutec.edutex.androidclient.conf.CustomApplication;
import de.dipf.edutec.edutex.androidclient.foreground.ForegroundNotificationCreator;
import de.dipf.edutec.edutex.androidclient.messageservice.MessageLayerSender;
import de.dipf.edutec.edutex.androidclient.utils.CommunitcationUtils;
import de.dipf.edutec.edutex.androidclient.sharedUtils.RequestResponseUtils;

enum TransferOption {
    TIMER,
    BATCH,
}

abstract class BaseSensorService extends Service implements SensorEventListener {

    /**
     * Diese Klasse ist dazu da um extended zu werden. Die public Attribute können
     * und sollen überschrieben werden. Anbhängig dieser werden Sensoren registriert
     * und weitere Eigenschaften festgelegt.
     */


    public String TAG = "";
    public String ServiceName = "";
    public double ServiceVersion = 0.0;
    private JSONObject request;
    private JSONObject original_request;
    private int fidelity;
    private int service_duration;

    // Collecting Sensor Data
    private SensorManager sensorManager = null;
    private ArrayList<JSONObject> events;
    public int[] sensorList = new int[]{};

    // Destroy failing Service
    private Boolean isStopping = false;
    private CountDownTimer countDownTimer;
    private long time_service_start;
    private long time_service_stop;

    // Transfering data continiously
    private Handler handler;
    private TransferOption transfer_option;


    // Transferpossibility time triggered
    public int interval_packets = 60;


    // Transferpossibility with batches
    public int no_events = 0;
    public int batch_size = 0;



    private Runnable transfer_packets_timer = new Runnable() {
        @Override
        public void run() {
            ArrayList<JSONObject> copy = new ArrayList<JSONObject>(events);
            events.clear();
            accumulate(copy);
            handler.postDelayed(this, 10 * 1000);
        }
    };

    private Runnable transfer_packets_batch = new Runnable() {
        @Override
        public void run() {
            ArrayList<JSONObject> copy = new ArrayList<JSONObject>(events);
            events.clear();
            accumulate(copy);
            handler.postDelayed(this, interval_packets * 1000);
        }
    };


    private MessageLayerSender messageLayerSender;


    public BaseSensorService(int[] sensorList, String ServiceName, double ServiceVersion, int batch_size, int transfer_packets_interval, String TAG, TransferOption option){
        super();
        this.TAG = TAG;
        this.sensorList = sensorList;
        this.ServiceName = ServiceName;
        this.ServiceVersion = ServiceVersion;
        this.batch_size = batch_size;
        this.interval_packets = transfer_packets_interval;
        this.transfer_option = option;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "onStartCommand called");
        ForegroundNotificationCreator fgNotificationCreator = ((CustomApplication) getApplication()).getContext().getForegroundNotificationCreator();
        startForeground(fgNotificationCreator.getId(), fgNotificationCreator.getNotification());
        messageLayerSender = new MessageLayerSender(getApplicationContext());

        events = new ArrayList<>();

        // Reading request informations

        if(intent.hasExtra("request")){
            String message = intent.getStringExtra("request");

            try {
                original_request = new JSONObject(intent.getStringExtra("original_request"));
                request = new JSONObject(message);
                fidelity = request.getInt("fidelity");
                service_duration = request.getInt("duration");

                Log.i(TAG, "Recording duration: " + String.valueOf(service_duration));

            } catch (JSONException e) {
                Log.e(TAG, e.toString());
            }
            Log.i(TAG, "Received Message: " + message);
        } else {
            Log.e(TAG, "Did not received the correct intent. Closing Service.");
            stopSelf();
        }


        // Registering all sensors
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        for(int i = 0; i < sensorList.length; i ++){
            if(sensorManager != null){
                Sensor sensor = sensorManager.getDefaultSensor(sensorList[i]);
                if(sensor != null){
                    Boolean isRegistered =  sensorManager.registerListener(this, sensor, fidelity);
                    Log.i(TAG,"Registration of Sensor " + String.valueOf(sensorList[i] + ": " + String.valueOf(isRegistered)));
                } else {
                    Log.d(TAG,"Sensor Type: " + String.valueOf(sensorList[i]) + " is not available");
                }
            }
        }

        Date date = new Date();
        time_service_start = date.getTime();
        time_service_stop = time_service_start + service_duration * 1000;

        // Sending continuously packets
        handler = new Handler();

        if(this.transfer_option == TransferOption.TIMER){
            handler.postDelayed(transfer_packets_timer, interval_packets * 1000);
        }


        // Stopping service #Way1
        countDownTimer = new CountDownTimer(service_duration * 1250, 1500) {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {

                stopSelf();
                Log.d(TAG, "Stopped Service");
            }
        }.start();

        return START_NOT_STICKY;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Instant instant = Instant.now();
        long elapsed = SystemClock.elapsedRealtimeNanos();

        if(new Date().getTime() >= time_service_stop && !isStopping){

            stopSelf();
            return;
        }

        JSONObject tmp = new JSONObject();
        try{
            //tmp.put("service_name",ServiceName);
            //tmp.put("service_version",ServiceVersion);
            //tmp.put("sensor_name",sensorEvent.sensor.getName());
            tmp.put("sensor_type", sensorEvent.sensor.getType());
            //tmp.put("datasource","wear");
            tmp.put("timestamp", RequestResponseUtils.getTimeStamps(instant,elapsed));
            tmp.put("values",valuesToArray(sensorEvent.values, sensorEvent.accuracy, sensorEvent.timestamp));
            tmp.put("sensor_service_id",request.getInt("id"));
            //tmp.put("service_type",request.getInt("service_type"));
            tmp.put("request_id",original_request.getInt("id"));
            //tmp.put("request_type", original_request.getString("request_type"));
            tmp.put("event_id",original_request.getString("event_id"));

            events.add(tmp);
        }catch (Exception e){}

        if(this.transfer_option == TransferOption.BATCH){
            no_events += 1;
            if(no_events >= batch_size){
                handler.post(transfer_packets_batch);
            }
        }

    }

    protected void accumulate(ArrayList<JSONObject> events_copy){

        if(events_copy.size() > 0 ){
            JSONObject packet = new JSONObject();
            ArrayList<JSONObject> eventsarray = new ArrayList<>();
            for(int i =0; i< events_copy.size(); i++){
                eventsarray.add(events_copy.get(i));
                if(i == events_copy.size() - 1 || i % 1500 == 0){
                    try{
                        packet.put("request_type",original_request.getString("request_type"));
                        packet.put("events",new JSONArray(eventsarray));
                        packet.put("session_id",original_request.getString("session_id"));
                        messageLayerSender.sendAck(CommunitcationUtils.TOHANDHELD_RESPONSE_SENSOR_REQUEST, packet.toString());
                        packet = new JSONObject();
                        eventsarray = new ArrayList<>();
                    } catch (Exception er){}
                }
            }
        }
    }

    @Override
    public void onDestroy(){
        Log.i(TAG,"onDestroy is called");

        try{
            if(sensorManager != null){
                sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
                if(sensorManager != null) {
                    sensorManager.unregisterListener(this);
                }
            }

        }catch (Exception er){
            Log.e(TAG, "Unregistration failed");
        }
        try{
            isStopping = true;
            handler.removeCallbacks(transfer_packets_timer);
            accumulate(events);

            countDownTimer.cancel();
        } catch (Exception exception){
            Log.e(TAG, exception.toString());
        }



        events.clear();
        stopForeground(false);
        super.onDestroy();
    }

    // Not used at the moment
    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public JSONObject valuesToArray(float[] values, int accuracy, long timestamp){
        JSONObject myValues = new JSONObject();
        ArrayList<Float> myArray = new ArrayList<Float>();
        for(int i = 0; i < values.length; i++){
            myArray.add(values[i]);
        }
        try {
            myValues.put("values",new JSONArray(myArray));
            myValues.put("accuracy",accuracy);
            myValues.put("event_timestamp",timestamp);
            return myValues;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return myValues;
    }

}
