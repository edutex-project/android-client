package de.dipf.edutec.edutex.androidclient.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import de.dipf.edutec.edutex.androidclient.messageservice.OffBodyRecordingService;
import de.dipf.edutec.edutex.androidclient.processtracking.ProcessTrackingService;
import de.dipf.edutec.edutex.androidclient.sensorsnapshotservices.AmbientNoiseRecordingService;
import de.dipf.edutec.edutex.androidclient.sensorsnapshotservices.BehavioralRecordingService;
import de.dipf.edutec.edutex.androidclient.sensorsnapshotservices.PhysicalRecordingService;
import de.dipf.edutec.edutex.androidclient.sensorsnapshotservices.PhysiologicalRecordingService;
import de.dipf.edutec.edutex.androidclient.sensorsnapshotservices.VirtualSensorService;

public class ServiceRunningUtil {

    public static Boolean  isMyServiceRunning(Class<?> serviceClass, Context context){
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    public static void terminate_services(Context context){

        if(isMyServiceRunning(ProcessTrackingService.class, context)){
            Intent intent0 = new Intent(context, ProcessTrackingService.class);
            context.stopService(intent0);
        }

        if(isMyServiceRunning(PhysiologicalRecordingService.class, context)){
            Intent intent0 = new Intent(context, PhysiologicalRecordingService.class);
            context.stopService(intent0);
        }

        if (isMyServiceRunning(BehavioralRecordingService.class, context)) {
            Intent intent0 = new Intent(context, BehavioralRecordingService.class);
            context.stopService(intent0);
        }

        if (isMyServiceRunning(PhysicalRecordingService.class, context)) {
            Intent intent0 = new Intent(context, PhysicalRecordingService.class);
            context.stopService(intent0);
        }

        if (isMyServiceRunning(AmbientNoiseRecordingService.class, context)) {
            Intent intent0 = new Intent(context, AmbientNoiseRecordingService.class);
            context.stopService(intent0);
        }

        if(isMyServiceRunning(VirtualSensorService.class, context)){
            Intent intent = new Intent(context, VirtualSensorService.class);
            context.stopService(intent);
        }

    }

    public static boolean areServiceClosed(Context context){

        String TAG = "ServiceRunningUtil";
        Log.d(TAG,"called");
        Class[] classes = new Class[] {
                ProcessTrackingService.class,
                PhysiologicalRecordingService.class,
                BehavioralRecordingService.class,
                PhysicalRecordingService.class,
                AmbientNoiseRecordingService.class,
                VirtualSensorService.class,
        };
        boolean response = true;
        for(int i=0; i < classes.length; i++){
            if(isMyServiceRunning(classes[i], context)){
                Log.e(TAG, classes[i].getSimpleName());
                response = false;
                break;
            }
        }
        return response;

    }



}
