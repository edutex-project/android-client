package de.dipf.edutec.thriller.experiencesampling;

import org.junit.Test;
import org.testng.annotations.AfterTest;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.assertEquals;

public class TimestampTest {

    @Test
    public void gettingIsoFormattedTimeStamp() {

        Instant timestamp = Instant.now();
        String timestampString = DateTimeFormatter.ISO_INSTANT.format(timestamp);
        System.out.println("Timestamp 01: " + timestampString);

        DateTimeFormatter f = DateTimeFormatter.ISO_INSTANT.withZone(ZoneId.systemDefault());
        ZonedDateTime zdt = ZonedDateTime.parse(timestampString, f);
        System.out.println("Timestamp 02: " + zdt);

        assertEquals(timestamp, zdt.toInstant());

//        ZonedDateTime zdt = ZonedDateTime.now();
//        System.out.println(zdt.format(DateTimeFormatter.ISO_INSTANT));
    }

//    @Test
//    public void parsingIsoFormattedTimeStamp() {
//
//        String timestamp = "2020-12-09T13:03:52.696Z";
//
//        DateTimeFormatter f1 = DateTimeFormatter.ISO_DATE_TIME;
//        ZonedDateTime zdt1 = ZonedDateTime.parse(timestamp, f1);
//        System.out.println("zdt1 = " + zdt1.toString());
//
//        DateTimeFormatter f2 = DateTimeFormatter.ISO_INSTANT.withZone(ZoneId.systemDefault());
//        ZonedDateTime zdt2 = ZonedDateTime.parse(timestamp, f2);
//        System.out.println("zdt2 = " + zdt2.toString());
//    }
}
