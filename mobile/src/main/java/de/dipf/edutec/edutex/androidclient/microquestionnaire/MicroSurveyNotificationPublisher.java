package de.dipf.edutec.edutex.androidclient.microquestionnaire;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import org.json.JSONException;
import org.json.JSONObject;
import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.activities.MainActivity;
import de.dipf.edutec.edutex.androidclient.activities.MicroQuestionnaireActivity;
import de.dipf.edutec.edutex.androidclient.messageservice.MessagesSingleton;

public class MicroSurveyNotificationPublisher  extends BroadcastReceiver {

    final static String TAG = "MicroSurveyNotificationPublisher";


    @Override
    public void onReceive(Context context, Intent intent) {

        Log.d(TAG, "onReceive is called");

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        MicroQuestionnairePreference prefs = new MicroQuestionnairePreference(context.getApplicationContext());
        String CHANNEL_ID = context.getResources().getString(R.string.notiChannelID);

        if(intent.hasExtra("message")){
            String received_message = intent.getStringExtra("message");


            try {
                JSONObject object = new JSONObject(received_message);
                prefs.setCurrentMicroQuestionnaire(object, object.hashCode());
                Intent sendIntent = new Intent(context, MicroQuestionnaireActivity.class);

                PendingIntent pendingIntent = TaskStackBuilder.create(context.getApplicationContext())
                        .addNextIntent(sendIntent)
                        .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

                Notification notification =  new NotificationCompat.Builder(context,CHANNEL_ID)
                        .setContentTitle("uSurvey")
                        .setSmallIcon(R.drawable.ic_cc_checkmark)
                        .setContentIntent(pendingIntent)
                        .build();

                notification.flags |= Notification.FLAG_AUTO_CANCEL;

                notificationManager.notify(prefs.getNotificationTag(), object.hashCode(), notification);

                Log.d(TAG, "Should notified");

                Intent interrupt_intent = new Intent();
                interrupt_intent.setAction(context.getString(R.string.ACTION_MICRO_SURVEY_INTERRUPT));
                LocalBroadcastManager.getInstance(context).sendBroadcast(interrupt_intent);

                Log.d(TAG, notificationManager.getActiveNotifications().toString());


            } catch (JSONException e) {
                e.printStackTrace();
            }



        } else if (intent.hasExtra("remove_message")){
            Log.d(TAG, "Command remove_message received");

            try {
                notificationManager.cancel(prefs.getNotificationTag(), prefs.getNotificationID());
                prefs.clear();

                Log.d(TAG,"Removed Notification for uSurvey");

                if(intent.hasExtra("acknowledge_not_answered")){
                    String mSurvey = intent.getStringExtra("remove_message");
                    JSONObject jsonObject = new JSONObject(mSurvey);
                    jsonObject.put("user_answer",-1);
                    MessagesSingleton.getInstance().addMessageReceived(jsonObject);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else if( intent.hasExtra("renotify")){
            Log.d(TAG,"Command renotify received");

            Intent sendIntent = new Intent(context, MainActivity.class);

            PendingIntent pendingIntent = TaskStackBuilder.create(context.getApplicationContext())
                    .addNextIntent(sendIntent)
                    .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

            Notification notification =  new NotificationCompat.Builder(context,CHANNEL_ID)
                    .setContentTitle("uSurvey")
                    .setSmallIcon(R.drawable.ic_cc_checkmark)
                    .setContentIntent(pendingIntent)
                    .build();

            notification.flags |= Notification.FLAG_AUTO_CANCEL;

            notificationManager.notify(prefs.getNotificationTag(),prefs.getNotificationID(), notification);
            prefs.updateCurrentMicroQuestionnaire();
        }




    }
}
