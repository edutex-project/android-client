package de.dipf.edutec.edutex.androidclient.microquestionnaire;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.ArrayList;

import de.dipf.edutec.edutex.androidclient.R;

public class MicroQuestionnaireSchedulingService extends Service {

    String TAG  = MicroQuestionnaireSchedulingService.class.getSimpleName();

    Handler handler;

    BroadcastReceiver receiver;
    BroadcastReceiver remove_receiver;
    ArrayList<Runnable> tasks;


    @Override
    public void onCreate(){
        Log.d(TAG,"created");
        tasks = new ArrayList<>();
    }


    private Runnable createRunnable(Intent intent){
        Runnable aRunnable = new Runnable() {
            @Override
            public void run() {
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                tasks.remove(this);
                Log.d(TAG,"runned job");
            }
        };

        tasks.add(aRunnable);

        return aRunnable;
    }


    private Runnable stop_service = new Runnable() {
        @Override
        public void run() {
            if(tasks.size() == 0){
                stopSelf();
            }

        }
    };


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG,"started");
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                intent.setAction(context.getString(R.string.ACTION_MICRO_SURVEY));
                handler.postDelayed(createRunnable(intent),60*1000);
                Log.d(TAG,"added job");
            }
        };

        remove_receiver = new BroadcastReceiver(){
            @Override
            public void onReceive(Context context, Intent intent) {
                for(int i = 0; i < tasks.size(); i++){
                    handler.removeCallbacks(tasks.get(i));
                }
                tasks.clear();
                handler.postDelayed(stop_service,10000);
                Log.d(TAG, "removed jobs");
            }
        };

        handler = new Handler();
        IntentFilter intentFilter = new IntentFilter("micro_scheduling_add");
        IntentFilter intentFilter1 = new IntentFilter("micro_scheduling_remove");
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver,intentFilter);
        LocalBroadcastManager.getInstance(this).registerReceiver(remove_receiver,intentFilter1);
        return START_NOT_STICKY;
    }





    public void onDestroy(){
        Log.d(TAG,"destroyed");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(remove_receiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        tasks.clear();

        super.onDestroy();
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
