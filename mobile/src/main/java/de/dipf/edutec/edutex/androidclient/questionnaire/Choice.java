package de.dipf.edutec.edutex.androidclient.questionnaire;

import java.util.ArrayList;

public class Choice {
    private String mChoice;

    public Choice(String choice){
        mChoice = choice;
    }

    public String getChoice(){return mChoice; }

    private static int lastChoiceID = 0;
    public static ArrayList<Choice> createChoiceList(int numContacts){
        ArrayList<Choice> choices = new ArrayList<Choice>();

        for(int i = 1; i <= numContacts; i++){
            choices.add(new Choice(String.valueOf(++lastChoiceID)));
        }
        return choices;
    }

}
