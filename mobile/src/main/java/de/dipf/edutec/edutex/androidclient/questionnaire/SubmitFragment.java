package de.dipf.edutec.edutex.androidclient.questionnaire;

import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.fragment.app.Fragment;

import org.json.JSONObject;

import java.time.Instant;
import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.activities.QuestionnaireActivity;
import de.dipf.edutec.edutex.androidclient.messageservice.MessagesSingleton;
import de.dipf.edutec.edutex.androidclient.util.SessionPreferencesUtil;
import lombok.SneakyThrows;
import de.dipf.edutec.edutex.androidclient.sharedUtils.RequestResponseUtils;


public class SubmitFragment extends Fragment {

    private JSONObject responseSurvey;
    private String TAG = SubmitFragment.class.getSimpleName();

    public SubmitFragment(JSONObject editedSurvey) {
        this.responseSurvey = editedSurvey;
    }


    @Override
    public void onStart(){
        super.onStart();
        ImageButton submit = getView().findViewById(R.id.bt_submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @SneakyThrows
            @Override
            public void onClick(View v) {
                Instant now = Instant.now();
                long elapsed = SystemClock.elapsedRealtimeNanos();
                if(responseSurvey.has("request_type")){
                    if(responseSurvey.get("request_type").equals("pre_post_questionnaire")){
                        if(responseSurvey.getInt("questionnaire_type") == 2){
                            // Telling the whole application that the session just started.
                            SessionPreferencesUtil sessionPreferences = new SessionPreferencesUtil(getActivity().getApplicationContext());
                            sessionPreferences.setSessionRunning(true);
                            sessionPreferences.setSPSessionTimer(System.currentTimeMillis() / 1000);
                            MessagesSingleton.getInstance().RequestSessionStart();
                        }

                    }
                }

                responseSurvey.put("timestamp_question_answered", RequestResponseUtils.getTimeStamps(now,elapsed));

                MessagesSingleton.getInstance().addMessageReceived(responseSurvey);

                Log.d(TAG, responseSurvey.toString());

                ((QuestionnaireActivity)getActivity()).surveyFinished = true;
                ((QuestionnaireActivity)getActivity()).finish();



            }
        });
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_submit, container, false);
        return view;
    }
}
