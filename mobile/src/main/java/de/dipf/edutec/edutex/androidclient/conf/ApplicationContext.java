package de.dipf.edutec.edutex.androidclient.conf;

import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;

import org.jetbrains.annotations.Nullable;

import java.util.UUID;

import de.dipf.edutec.edutex.androidclient.activities.MainActivity;
import de.dipf.edutec.edutex.androidclient.foreground.ForegroundNotificationCreator;
import de.dipf.edutec.edutex.androidclient.sensorservice.transport.CustomCaSslSocketFactoryWrapper;
import de.dipf.edutec.edutex.androidclient.sensorservice.transport.SslSocketFactoryWrapper;
import de.dipf.edutec.edutex.androidclient.sensorservice.transport.UnsafeSslSocketFactoryWrapper;
import lombok.Getter;

@Getter
public class ApplicationContext {

    private static final boolean DEVELOPMENT = true;
    private static final String TAG = ApplicationContext.class.getSimpleName();
    private static final String CHANNEL_ID = "ForegroundServiceChannel";
    private static final String NOTIFICATION_CONTENT_TITLE = "";
    private static final String NOTIFICATION_CONTENT_TEXT = "Tap to return to App";

    //private final MqttService mqttService;
    //private final ESMService esmService;

    private final ForegroundNotificationCreator foregroundNotificationCreator;

    public ApplicationContext(Context ctx, NotificationManager notificationManager) {

        /*
        MqttConnectOptions connOpts = new MqttConnectOptions();
        connOpts.setCleanSession(true);
        connOpts.setAutomaticReconnect(true);
        connOpts.setConnectionTimeout(10);

        SslSocketFactoryWrapper sslSocketFactoryWrapper = createSslSocketFactoryWrapper(ctx);
        // if null, there was no certificate provided in PROD, so only the trusted android certificates will be allowed
        if(sslSocketFactoryWrapper != null) {
            connOpts.setSocketFactory(sslSocketFactoryWrapper.create());
            connOpts.setSSLHostnameVerifier((hostname, session) -> true);
        }*/

        this.foregroundNotificationCreator = new ForegroundNotificationCreator(
                1,
                CHANNEL_ID,
                ctx,
                MainActivity.class,
                notificationManager,
                NOTIFICATION_CONTENT_TITLE,
                NOTIFICATION_CONTENT_TEXT,
                de.dipf.edutec.edutex.androidclient.shared.R.drawable.ic_notifications_black_24dp);

        /*
        //String clientID = getUuid(ctx).toString();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ctx.getApplicationContext());
        String userID = preferences.getString(ctx.getString(R.string.key_general_user_id),"");
        SharedPreferences preferences1 = ctx.getSharedPreferences(ctx.getPackageName(), Context.MODE_PRIVATE);
        preferences1.edit().putString("UUID",userID).apply();

        //.clientId(clientID)
        mqttService = new MqttService(
                MqttClientBuilder.builder()
                        .clientId(userID)
                        .build()
                        .build(),
                connOpts);

        Log.d(TAG,"Used clienID is: " + userID);
        Toast.makeText(ctx,"Used Client ID: " + userID, Toast.LENGTH_LONG).show();
        //esmService = new ESMService();

         */
    }

    private UUID getUuid(Context ctx) {
        UUID clientId;
        SharedPreferences pref = ctx.getSharedPreferences(ctx.getPackageName(), Context.MODE_PRIVATE);
        String string = pref.getString("UUID", null);
        if (string == null) {
            clientId = UUID.randomUUID();
            pref.edit().putString("UUID", clientId.toString()).apply();
        } else {
            clientId = UUID.fromString(string);
        }
        return clientId;
    }

    @Nullable
    private SslSocketFactoryWrapper createSslSocketFactoryWrapper(Context ctx) {
        SslSocketFactoryWrapper socketFactoryFactory = null;

        if (DEVELOPMENT) {
            socketFactoryFactory = new UnsafeSslSocketFactoryWrapper();
        } else {
            int raw = ctx.getResources().getIdentifier("ca", "raw", ctx.getPackageName());
            if (raw != 0) {
                socketFactoryFactory = new CustomCaSslSocketFactoryWrapper(ctx, raw);
            }
        }
        return socketFactoryFactory;
    }

    public ForegroundNotificationCreator getForegroundNotificationCreator() {
        return foregroundNotificationCreator;
    }
}
