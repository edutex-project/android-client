package de.dipf.edutec.edutex.androidclient.processtracking;

import android.app.usage.UsageEvents;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.os.Build;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.dipf.edutec.edutex.androidclient.messageservice.MessagesSingleton;
import de.dipf.edutec.edutex.androidclient.util.TrackingSensorPreferences;
import de.dipf.edutec.edutex.androidclient.sharedUtils.RequestResponseUtils;

public class UStats {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("M-d-yyyy HH:mm:ss");
    public static final String TAG = UStats.class.getSimpleName();


    @SuppressWarnings("ResourceType")
    public static void getStats(Context context) {
        UsageStatsManager usm = (UsageStatsManager) context.getSystemService(Context.USAGE_STATS_SERVICE);
        int interval = UsageStatsManager.INTERVAL_YEARLY;
        Calendar calendar = Calendar.getInstance();
        long endTime = calendar.getTimeInMillis();
        calendar.add(Calendar.YEAR, -1);
        long startTime = calendar.getTimeInMillis();

        Log.d(TAG, "Range start:" + dateFormat.format(startTime));
        Log.d(TAG, "Range end:" + dateFormat.format(endTime));

        UsageEvents uEvents = usm.queryEvents(startTime, endTime);
        while (uEvents.hasNextEvent()) {
            UsageEvents.Event e = new UsageEvents.Event();
            uEvents.getNextEvent(e);

            if (e != null) {
                Log.d(TAG, "Event: " + e.getPackageName() + "\t" + e.getTimeStamp());
            }
        }
    }

    public static void getUsageStatsList(Context context) {

        Instant now = Instant.now();
        long elapsed = SystemClock.elapsedRealtimeNanos();

        String ServiceName = "Usage Stats Service";
        double ServiceVersion = 1.0;

        UsageStatsManager usm = getUsageStatsManager(context);
        Calendar calendar = Calendar.getInstance();
        long endTime = calendar.getTimeInMillis();
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        long startTime = calendar.getTimeInMillis();

        Log.d(TAG, "Range start:" + dateFormat.format(startTime));
        Log.d(TAG, "Range end:" + dateFormat.format(endTime));

        List<UsageStats> usageStatsList = usm.queryUsageStats(UsageStatsManager.INTERVAL_BEST, startTime, endTime);

        JSONArray array = parseUsageStatsList(usageStatsList);

        TrackingSensorPreferences trackingSensorPreferences = new TrackingSensorPreferences(context);
        JSONObject request = trackingSensorPreferences.getTrackRequest();
        JSONObject original = trackingSensorPreferences.getTrackOriginalRequest();

        JSONObject tmp = new JSONObject();
        try {
            tmp.put("service_name", ServiceName);
            tmp.put("service_version", ServiceVersion);
            tmp.put("sensor_name", "UsageStatsManager");
            tmp.put("sensor_type", -6);
            tmp.put("datasource", "mobile");
            tmp.put("timestamp", RequestResponseUtils.getTimeStamps(now, elapsed));
            tmp.put("values", array);
            tmp.put("sensor_service_id", request.getInt("id"));
            tmp.put("service_type", request.getInt("service_type"));
            tmp.put("request_id", original.getInt("id"));
            tmp.put("request_type", original.getString("request_type"));
            tmp.put("event_id", original.getString("event_id"));


            JSONArray array2 = new JSONArray();
            array2.put(tmp);

            JSONObject packet = new JSONObject();
            packet.put("events", array2);
            packet.put("request_type", "sensor_service");
            packet.put("session_id", original.getString("session_id"));

            MessagesSingleton.getInstance().addMessageReceived(packet);

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

    }

    public static JSONArray parseUsageStatsList(List<UsageStats> usageStatsList) {
        JSONArray jsonArray = new JSONArray();
        for (UsageStats u : usageStatsList) {
            JSONObject jsonObject = new JSONObject();

            try {
                jsonObject.put("datasource", "mobile");
                jsonObject.put("package_name", u.getPackageName());
                jsonObject.put("first_timestamp", u.getFirstTimeStamp());
                jsonObject.put("last_timestamp", u.getLastTimeUsed());
                jsonObject.put("last_time_used", u.getLastTimeUsed());
                jsonObject.put("total_time_in_foreground", u.getTotalTimeInForeground());

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    jsonObject.put("last_time_foreground_service_used", u.getLastTimeForegroundServiceUsed());
                    jsonObject.put("last_time_visible", u.getLastTimeVisible());
                    jsonObject.put("total_time_foreground_service_used", u.getTotalTimeForegroundServiceUsed());
                    jsonObject.put("total_time_visible", u.getTotalTimeVisible());
                } else {
                    jsonObject.put("last_time_foreground_service_used", "Android Version to low.");
                    jsonObject.put("last_time_visible", "Android Version to low.");
                    jsonObject.put("total_time_foreground_service_used", "Android Version to low.");
                    jsonObject.put("total_time_visible", "Android Version to low.");
                }
                Log.d(TAG, jsonObject.toString(4));
                jsonArray.put(jsonObject);
            } catch (Exception er) {
                Log.d(TAG, "Error occurred while parsing.");
            }
        }
        return jsonArray;
    }

    @SuppressWarnings("ResourceType")
    private static UsageStatsManager getUsageStatsManager(Context context) {
        UsageStatsManager usm = (UsageStatsManager) context.getSystemService(Context.USAGE_STATS_SERVICE);
        return usm;
    }

    public static void getUsageStatistics(long start_time, long end_time, Context context) {

        UsageEvents.Event currentEvent;

        HashMap<String, AppUsageInfo> map = new HashMap<>();
        HashMap<String, List<UsageEvents.Event>> sameEvents = new HashMap<>();

        UsageStatsManager mUsageStatsManager = (UsageStatsManager)
                context.getSystemService(Context.USAGE_STATS_SERVICE);

        if (mUsageStatsManager != null) {
            // Get all apps data from starting time to end time
            UsageEvents usageEvents = mUsageStatsManager.queryEvents(start_time, end_time);

            // Put these data into the map
            while (usageEvents.hasNextEvent()) {
                currentEvent = new UsageEvents.Event();
                usageEvents.getNextEvent(currentEvent);
                if (currentEvent.getEventType() == UsageEvents.Event.ACTIVITY_RESUMED ||
                        currentEvent.getEventType() == UsageEvents.Event.ACTIVITY_PAUSED) {
                    //  allEvents.add(currentEvent);
                    String key = currentEvent.getPackageName();
                    if (map.get(key) == null) {
                        map.put(key, new AppUsageInfo(key));
                        sameEvents.put(key, new ArrayList<UsageEvents.Event>());
                    }
                    sameEvents.get(key).add(currentEvent);
                }
            }

            // Traverse through each app data which is grouped together and count launch, calculate duration
            for (Map.Entry<String, List<UsageEvents.Event>> entry : sameEvents.entrySet()) {
                int totalEvents = entry.getValue().size();
                if (totalEvents > 1) {
                    for (int i = 0; i < totalEvents - 1; i++) {
                        UsageEvents.Event E0 = entry.getValue().get(i);
                        UsageEvents.Event E1 = entry.getValue().get(i + 1);

                        if (E1.getEventType() == 1 || E0.getEventType() == 1) {
                            map.get(E1.getPackageName()).launchCount++;
                        }

                        if (E0.getEventType() == 1 && E1.getEventType() == 2) {
                            long diff = E1.getTimeStamp() - E0.getTimeStamp();
                            map.get(E0.getPackageName()).timeInForeground += diff;
                        }
                    }
                }
                // If First eventtype is ACTIVITY_PAUSED then added the difference of start_time and Event occuring time because the application is already running.
                if (entry.getValue().get(0).getEventType() == 2) {
                    long diff = entry.getValue().get(0).getTimeStamp() - start_time;
                    map.get(entry.getValue().get(0).getPackageName()).timeInForeground += diff;
                }

                // If Last eventtype is ACTIVITY_RESUMED then added the difference of end_time and Event occuring time because the application is still running .
                if (entry.getValue().get(totalEvents - 1).getEventType() == 1) {
                    long diff = end_time - entry.getValue().get(totalEvents - 1).getTimeStamp();
                    map.get(entry.getValue().get(totalEvents - 1).getPackageName()).timeInForeground += diff;
                }
            }
        } else {
            Toast.makeText(context, "Sorry...", Toast.LENGTH_SHORT).show();
        }


        Instant now = Instant.now();
        long elapsed = SystemClock.elapsedRealtimeNanos();
        String ServiceName = "Usage Stats Events Aggregated";
        double ServiceVersion = 1.0;

        TrackingSensorPreferences trackingSensorPreferences = new TrackingSensorPreferences(context);
        JSONObject request = trackingSensorPreferences.getTrackRequest();
        JSONObject original = trackingSensorPreferences.getTrackOriginalRequest();


        JSONObject tmp = new JSONObject();
        try {
            tmp.put("service_name", ServiceName);
            tmp.put("service_version", ServiceVersion);
            tmp.put("sensor_name", "UsageStatsManager");
            tmp.put("sensor_type", -10);
            tmp.put("datasource", "mobile");
            tmp.put("timestamp", RequestResponseUtils.getTimeStamps(now, elapsed));
            tmp.put("values", parseQuerysageStatsList(map));
            tmp.put("sensor_service_id", request.getInt("id"));
            tmp.put("service_type", request.getInt("service_type"));
            tmp.put("request_id", original.getInt("id"));
            tmp.put("request_type", original.getString("request_type"));
            tmp.put("event_id", original.getString("event_id"));


            JSONArray array2 = new JSONArray();
            array2.put(tmp);

            JSONObject packet = new JSONObject();
            packet.put("events", array2);
            packet.put("request_type", "sensor_service");
            packet.put("session_id", original.getString("session_id"));

            MessagesSingleton.getInstance().addMessageReceived(packet);
        } catch (Exception e) {
        }


    }

    public static JSONArray parseQuerysageStatsList(Map<String, AppUsageInfo> usageStatsList) {

        JSONArray jsonArray = new JSONArray();
        for (Map.Entry<String, AppUsageInfo> entry : usageStatsList.entrySet()) {
            String key = entry.getKey();
            JSONObject u = entry.getValue().reformat();
            jsonArray.put(u);

        }
        return jsonArray;
    }

    public static void getAllEvents(long start_time, long end_time, Context context) {
        UsageEvents.Event currentEvent;

        HashMap<String, AppUsageInfo> map = new HashMap<>();
        HashMap<String, List<UsageEvents.Event>> sameEvents = new HashMap<>();

        UsageStatsManager mUsageStatsManager = (UsageStatsManager)
                context.getSystemService(Context.USAGE_STATS_SERVICE);
        JSONArray array = new JSONArray();
        if (mUsageStatsManager != null) {
            // Get all apps data from starting time to end time
            UsageEvents usageEvents = mUsageStatsManager.queryEvents(start_time, end_time);

            // Put these data into the map
            while (usageEvents.hasNextEvent()) {
                currentEvent = new UsageEvents.Event();
                usageEvents.getNextEvent(currentEvent);

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("event_type", currentEvent.getEventType());
                    jsonObject.put("package_name", currentEvent.getPackageName());
                    jsonObject.put("timestamp", currentEvent.getTimeStamp());
                    jsonObject.put("class_name", currentEvent.getClassName());
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        jsonObject.put("app_standby_bucket", currentEvent.getAppStandbyBucket());
                    } else {
                        jsonObject.put("app_standby_bucket", null);
                    }
                    jsonObject.put("shortcut_id", currentEvent.getShortcutId());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                array.put(jsonObject);


            }


        } else {
            Toast.makeText(context, "Sorry...", Toast.LENGTH_SHORT).show();
        }


        Instant now = Instant.now();
        long elapsed = SystemClock.elapsedRealtimeNanos();
        String ServiceName = "Usage Stats Events Aggregated";
        double ServiceVersion = 1.0;

        TrackingSensorPreferences trackingSensorPreferences = new TrackingSensorPreferences(context);
        JSONObject request = trackingSensorPreferences.getTrackRequest();
        JSONObject original = trackingSensorPreferences.getTrackOriginalRequest();


        JSONObject tmp = new JSONObject();
        try {
            tmp.put("service_name", ServiceName);
            tmp.put("service_version", ServiceVersion);
            tmp.put("sensor_name", "UsageStatsManager");
            tmp.put("sensor_type", -11);
            tmp.put("datasource", "mobile");
            tmp.put("timestamp", RequestResponseUtils.getTimeStamps(now, elapsed));
            tmp.put("values", array);
            tmp.put("sensor_service_id", request.getInt("id"));
            tmp.put("service_type", request.getInt("service_type"));
            tmp.put("request_id", original.getInt("id"));
            tmp.put("request_type", original.getString("request_type"));
            tmp.put("event_id", original.getString("event_id"));


            JSONArray array2 = new JSONArray();
            array2.put(tmp);

            JSONObject packet = new JSONObject();
            packet.put("events", array2);
            packet.put("request_type", "sensor_service");
            packet.put("session_id", original.getString("session_id"));

            MessagesSingleton.getInstance().addMessageReceived(packet);
        } catch (Exception e) {
        }


    }


}
