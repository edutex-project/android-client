package de.dipf.edutec.edutex.androidclient.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.google.android.material.internal.NavigationMenuItemView;

import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.util.ESMConfigurationPreferences;

public class PermissionDialog {

    private Dialog cDialog;
    private Context cContext;
    private Button button, stopReminding;
    TextView windowChange, notification, usageStats;
    Boolean activeNotification,activeUsageStats,activeWindow;
    Activity activity;

    public PermissionDialog(Context context, Boolean notificaionEnabled, Boolean appusageEnabled,
                            Boolean windowChangeEnabled, Activity activity){
        this.cContext =context;
        this.activity = activity;
        activeNotification = notificaionEnabled;
        activeUsageStats =appusageEnabled;
        activeWindow = windowChangeEnabled;

    }

    public void showDialog(){

        cDialog = new Dialog(cContext);

        /*Making the ProgressBar Background transparent*/
        cDialog.setContentView(R.layout.dialog_tracking_permissions);
        cDialog.setCancelable(true);
        cDialog.setCanceledOnTouchOutside(true);
        cDialog.show();

        findGUIElements();
    }



    private void findGUIElements(){
        windowChange = cDialog.findViewById(R.id.dialog_tracking_windowdetection);
        notification = cDialog.findViewById(R.id.dialog_tracking_notification);
        usageStats = cDialog.findViewById(R.id.dialog_tracking_usagestats);
        button = cDialog.findViewById(R.id.dialog_tracking_close);
        stopReminding = cDialog.findViewById(R.id.dialog_stop_reminding);

        stopReminding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //ESMConfigurationPreferences esmConfigurationPreferences = new ESMConfigurationPreferences(cContext.getApplicationContext());
                //esmConfigurationPreferences.setTrackingReminder(false);
                cDialog.dismiss();
                NavigationMenuItemView menuItemView = activity.findViewById(R.id.nav_item_tracking);
                menuItemView.callOnClick();
            }
        });


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cDialog.dismiss();
            }
        });

        if(activeNotification){
            notification.setText("enabled");
            notification.setTextColor(ContextCompat.getColor(cContext, R.color.green_ok));
        } else {
            notification.setText("disabled");
            notification.setTextColor(ContextCompat.getColor(cContext,R.color.red_ok));
        }

        if (activeUsageStats){
            usageStats.setText("enabled");
            usageStats.setTextColor(ContextCompat.getColor(cContext,R.color.green_ok));
        } else {
            usageStats.setText("disabled");
            usageStats.setTextColor(ContextCompat.getColor(cContext,R.color.red_ok));
        }

        if(activeWindow){
            windowChange.setText("enabled");
            windowChange.setTextColor(ContextCompat.getColor(cContext,R.color.green_ok));
        } else {
            windowChange.setText("disabled");
            windowChange.setTextColor(ContextCompat.getColor(cContext,R.color.red_ok));
        }



    }


    public void hideProgress(){
        if(cDialog != null){
            cDialog.dismiss();
            cDialog = null;
        }
    }

}

