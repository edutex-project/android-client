package de.dipf.edutec.edutex.androidclient.sensorservice;

import android.hardware.Sensor;


public class TemperatureRecordingService extends BaseSensorService {
    private static final String TAG = TemperatureRecordingService.class.getSimpleName();
    public static String ServiceName = "Behavioral Sensor Recording Service";
    public static double ServiceVersion = 1.0;


    private static int[] sensorList = new int[]{
            Sensor.TYPE_AMBIENT_TEMPERATURE,
    };

    public static int interval_packets = 30;
    public static int batch_size = 50;

    public TemperatureRecordingService() {
        super(sensorList, ServiceName, ServiceVersion,batch_size,  interval_packets, TAG, TransferOption.TIMER);
    }

}
