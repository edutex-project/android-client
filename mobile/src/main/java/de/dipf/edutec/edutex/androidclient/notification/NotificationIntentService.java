package de.dipf.edutec.edutex.androidclient.notification;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.RemoteInput;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.Instant;
import java.util.Objects;
import de.dipf.edutec.edutex.androidclient.R;

import de.dipf.edutec.edutex.androidclient.messageservice.MessagesSingleton;
import de.dipf.edutec.edutex.androidclient.sharedUtils.RequestResponseUtils;

public class NotificationIntentService extends IntentService {

    static String TAG = "NotificationIntentService";
    private static String TAG_ID = "NotificationPublisher";
    public NotificationIntentService(){
        super("NotificationPublisher");
    }




    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        String CHANNEL_ID = getApplicationContext().getResources().getString(R.string.notiChannelID);
        NotificationManager notificationManager = (NotificationManager)
                getApplicationContext().getSystemService(NOTIFICATION_SERVICE);

        NotificationCompat.Builder notification = new NotificationCompat.Builder(getApplicationContext(),CHANNEL_ID)
                .setContentText("Submitted")
                .setSmallIcon(R.drawable.ic_survey);

        notificationManager.notify(TAG_ID, intent.getIntExtra("publishing_id",0),notification.build());

        Instant instant = Instant.now();
        long elapsed = SystemClock.elapsedRealtimeNanos();
        if(intent.hasExtra("intent_destination") &&
                intent.getStringExtra("intent_destination")
                        .equals("NotificationIntentService")){

            Log.d(TAG, "received user input to digest");

            Bundle remoteInput = RemoteInput.getResultsFromIntent(intent);
            if(remoteInput != null){
                CharSequence userReply = remoteInput.getCharSequence(getString(R.string.key_result_intent_notification));
                Log.d(TAG, "user answer: " + userReply);
                if(userReply != null){

                    JSONObject modifiedMessage = null;
                    try {
                        modifiedMessage = new JSONObject(Objects.requireNonNull(intent.getStringExtra("original_esm_message")));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    try{
                        JSONArray answers = modifiedMessage.getJSONArray("notification_answer");
                        for(int i = 0; i < answers.length(); i++){
                            if(answers.getJSONObject(i).getString("answer").equals(userReply)){
                                modifiedMessage.put("user_answer",answers.getJSONObject(i).getInt("id"));
                                break;
                            }
                        }

                        modifiedMessage.put("timestamp_question_answered", RequestResponseUtils.getTimeStamps(instant,elapsed));

                        Log.d(TAG, "added user_answer and answering time to message");
                        MessagesSingleton.getInstance().addMessageReceived(modifiedMessage);
                        Log.d(TAG, "passed new message to Class MessageLayerSender");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            //notificationManager.cancel( TAG_ID, intent.getIntExtra("publishing_id",0));


        }
    }
}


