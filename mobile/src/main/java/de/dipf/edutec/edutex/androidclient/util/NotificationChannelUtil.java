package de.dipf.edutec.edutex.androidclient.util;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.util.Log;

import java.util.List;

import de.dipf.edutec.edutex.androidclient.R;

public class NotificationChannelUtil {

    private static final String TAG = NotificationChannelUtil.class.getSimpleName();


    public static void create_notification_channel(Context context){
        String channelID = context.getString(R.string.notiChannelID);
        String channelName = context.getString(R.string.notiChannelName);
        NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
        List<NotificationChannel> channels = notificationManager.getNotificationChannels();

        boolean channel_already_exists = false;

        for(int i = 0; i < channels.size(); i++){
            String channel_name = channels.get(i).getId();
            if(channel_name.equals(channelID)){
                channel_already_exists = true;
                break;
            }
        }

        if(! channel_already_exists){
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){

                int importance = NotificationManager.IMPORTANCE_HIGH;

                NotificationChannel channel = new NotificationChannel(channelID, channelName, importance);
                channel.enableVibration(true);
                channel.setVibrationPattern(new long[] {0, 1000, 0, 1000, 0, 1000, 0, 1000});

                notificationManager.createNotificationChannel(channel);
                Log.d(TAG, "NotificationChannel: " + channelName + " was created");
            }
        } else {
            Log.d(TAG, "NotificationChannel: " + channelName + " already exists. Do not need to recreate.");
        }
    }

}
