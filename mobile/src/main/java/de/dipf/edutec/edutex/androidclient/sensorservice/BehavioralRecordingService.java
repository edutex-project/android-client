package de.dipf.edutec.edutex.androidclient.sensorservice;

import android.hardware.Sensor;

public class BehavioralRecordingService extends BaseSensorService{

    private static final String TAG = BehavioralRecordingService.class.getSimpleName();
    public static String ServiceName = "Behavioral Sensor Recording Service";
    public static double ServiceVersion = 1.0;


    private static int[] sensorList = new int[]{
            Sensor.TYPE_ACCELEROMETER,
            Sensor.TYPE_GYROSCOPE,
            Sensor.TYPE_MAGNETIC_FIELD,
    };

    public static int interval_packets = 15;
    public static int batch_size = 100;

    public BehavioralRecordingService() {
        super(sensorList, ServiceName, ServiceVersion,batch_size,  interval_packets, TAG, TransferOption.TIMER);
    }

}
