package de.dipf.edutec.edutex.androidclient.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.preference.PreferenceManager;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.util.ESMConfigurationPreferences;

public class LoginSettingsDialog {


    private Dialog cDialog;
    private Context cContext;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;


    CheckBox deploymentBox, developmentBox;
    TextInputLayout portLayout;
    TextInputEditText hostInput, portInput;
    TextInputEditText moodleID;
    TextView errorView;

    CheckBox connectionHTTP, connectionHTTPS;
    CheckBox websocketWS, websocketWSS;
    Button saveButton, cancelButton;


    public LoginSettingsDialog(Context context){
        this.cContext = context;
        this.sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        this.editor = this.sharedPreferences.edit();

    }


    public void showDialog(){
        cDialog = new Dialog(cContext);

        /*Making the ProgressBar Background transparent*/
        cDialog.setContentView(R.layout.dialog_login_settings);
        cDialog.setCancelable(false);
        cDialog.setCanceledOnTouchOutside(false);
        cDialog.show();

        findGUIElements();
        setPreferencesValues();

    }

    private void setPreferencesValues(){


        ESMConfigurationPreferences esmConfigurationPreferences = new ESMConfigurationPreferences(cContext);


        if(esmConfigurationPreferences.getIsHttps()){
            connectionHTTPS.setChecked(true);
            connectionHTTP.setChecked(false);
        } else{
            connectionHTTPS.setChecked(false);
            connectionHTTP.setChecked(true);
        }

        if(esmConfigurationPreferences.getIsWss()){
            websocketWSS.setChecked(true);
            websocketWS.setChecked(false);
        }else{
            websocketWSS.setChecked(false);
            websocketWS.setChecked(true);
        }

        connectionHTTP.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    connectionHTTPS.setChecked(false);
                    connectionHTTP.setChecked(true);
                } else {
                    if(!connectionHTTPS.isChecked()){
                        connectionHTTPS.setChecked(true);
                    }
                }

            }
        });

        connectionHTTPS.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    connectionHTTPS.setChecked(true);
                    connectionHTTP.setChecked(false);
                }else {
                    if(!connectionHTTP.isChecked()){
                        connectionHTTP.setChecked(true);
                    }
                }

            }
        });


        websocketWS.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    websocketWSS.setChecked(false);
                    websocketWS.setChecked(true);
                } else {
                    if(!websocketWSS.isChecked()){
                        websocketWSS.setChecked(true);
                    }
                }

            }
        });

        websocketWSS.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    websocketWSS.setChecked(true);
                    websocketWS.setChecked(false);
                }else{
                    if(!websocketWS.isChecked()){
                        websocketWS.setChecked(true);
                    }
                }
            }
        });



    }


    private void findGUIElements(){
        connectionHTTP = cDialog.findViewById(R.id.login_sett_http);
        connectionHTTPS = cDialog.findViewById(R.id.login_sett_https);

        websocketWS = cDialog.findViewById(R.id.login_sett_ws);
        websocketWSS = cDialog.findViewById(R.id.login_sett_wss);

        portInput = cDialog.findViewById(R.id.login_settings_port);

        moodleID = cDialog.findViewById(R.id.login_settings_moodle_id);

        ESMConfigurationPreferences esmConfigurationPreferences = new ESMConfigurationPreferences(cContext);
        String port = esmConfigurationPreferences.getPort();
        if(port != null){portInput.setText(port);}
        String moodle = esmConfigurationPreferences.getMoodleId();
        if(moodle != null){moodleID.setText(moodle);}




        saveButton = cDialog.findViewById(R.id.login_settings_save);
        cancelButton = cDialog.findViewById(R.id.login_settings_cancel);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ESMConfigurationPreferences esmConfigurationPreferences = new ESMConfigurationPreferences(cContext);
                esmConfigurationPreferences.set_port(portInput.getText().toString());

                String moodle_id = moodleID.getText().toString();
                if(moodle_id != ""){
                    esmConfigurationPreferences.setMoodleId(moodle_id);
                }


                String toast_message = "";
                if(connectionHTTP.isChecked()){
                    esmConfigurationPreferences.set_is_https(false);
                    toast_message += "Using http ";
                } else {
                    esmConfigurationPreferences.set_is_https(true);
                    toast_message += "Using https ";
                }

                if(websocketWS.isChecked()){
                    esmConfigurationPreferences.set_is_wss(false);
                    toast_message += "and ws settings.";
                } else {
                    esmConfigurationPreferences.set_is_wss(true);
                    toast_message += "and wss settings.";
                }

                Toast.makeText(cContext, "Changes saved. \n" + toast_message, Toast.LENGTH_LONG).show();
                cDialog.dismiss();

            }
        });



        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cDialog.dismiss();
                Toast.makeText(cContext, "Nothing changed.", Toast.LENGTH_LONG).show();
            }
        });

    }


    public void hideProgress(){
        if(cDialog != null){
            cDialog.dismiss();
            cDialog = null;
        }
    }

}
