package de.dipf.edutec.edutex.androidclient.boot;

import static de.dipf.edutec.edutex.androidclient.notification.NotificationPublisher.flagCancelCurrent;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;


import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.android.gms.wearable.CapabilityClient;
import com.google.android.gms.wearable.Wearable;

import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.activities.MainActivity;
import de.dipf.edutec.edutex.androidclient.auth.AuthPreferences;
import de.dipf.edutec.edutex.androidclient.conf.CustomApplication;
import de.dipf.edutec.edutex.androidclient.conf.ReceiversManager;
import de.dipf.edutec.edutex.androidclient.messageservice.MessagesSingleton;
import de.dipf.edutec.edutex.androidclient.util.ESMConfigurationPreferences;
import de.dipf.edutec.edutex.androidclient.util.SessionPreferencesUtil;
import de.dipf.edutec.edutex.androidclient.foreground.ForegroundNotificationCreator;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class RunAfterBootService extends Service {


    private static final String TAG = RunAfterBootService.class.getSimpleName();
    private static final String SUFFIX = "/participants/status";
    String url= null;

    ESMConfigurationPreferences esmConfigurationPreferences;
    SessionPreferencesUtil sessionPreferencesUtil;
    AuthPreferences authPreferences;
    OkHttpClient okHttpClient;
    Handler handler;


    long timerGetRequest = 10000;
    int reminderLoginTime = 1;
    int countReminded = 0;




    Runnable checkSessionRunning = new Runnable() {
        @Override
        public void run() {
            update_participant_status();
        }
    };

    Runnable checkClientLogin = new Runnable() {
        @Override
        public void run() {
            if(authPreferences.getAuthToken() == null){
                buildLoginReminder();
                countReminded += 1;
                handler.postDelayed(this, 2 * countReminded * 1000 * 60 );
            }
        }
    };


    public void buildLoginReminder(){
        String CHANNEL_ID = getApplicationContext().getResources().getString(R.string.notiChannelID);
        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent directReplyPendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, flagCancelCurrent);

        NotificationCompat.Builder notification = new NotificationCompat.Builder(getApplicationContext(),CHANNEL_ID)
                .setContentTitle("Missing Edutex Login!")
                .setContentText("Please login to the edutex client.")
                .setSmallIcon(R.drawable.icon_home)
                .setContentIntent(directReplyPendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify("ReminderLogin",404,notification.build());
    }

    public RunAfterBootService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "RunAfterBootService onCreate() method.");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "RunAfterBootService onStartCommand() method.");

        ForegroundNotificationCreator fgNotificationCreator = ((CustomApplication) getApplication()).getContext().getForegroundNotificationCreator();
        startForeground(fgNotificationCreator.getId(), fgNotificationCreator.getNotification());

        esmConfigurationPreferences = new ESMConfigurationPreferences(getApplicationContext());
        sessionPreferencesUtil = new SessionPreferencesUtil(getApplicationContext());
        authPreferences = new AuthPreferences(getApplicationContext());


        handler = new Handler();
        handler.postDelayed(checkSessionRunning,1000);
        handler.postDelayed(checkClientLogin, 1000);

        ReceiversManager.getInstance().registerCapabilityClient(getApplicationContext());
        ReceiversManager.getInstance().registerReceivers(getApplicationContext());

        Wearable.getCapabilityClient(getApplicationContext())
                .getCapability("bluetooth-status-mobile", CapabilityClient.FILTER_REACHABLE);

        return super.onStartCommand(intent, flags, startId);
    }


    public void update_participant_status(){

        try{
            Log.d(TAG,"GET Partcipant Status");
            Boolean isSessionRunning = sessionPreferencesUtil.getSessionRunning();


            okHttpClient = new OkHttpClient().newBuilder()
                    .callTimeout(20,TimeUnit.SECONDS)
                    .readTimeout(20, TimeUnit.SECONDS)
                    .writeTimeout(20,TimeUnit.SECONDS)
                    .connectTimeout(20, TimeUnit.SECONDS)
                    .build();


                    //.Builder()
                    //.connectTimeout(100, TimeUnit.SECONDS).writeTimeout(20, TimeUnit.SECONDS).readTimeout(20, TimeUnit.SECONDS).build();
            url = esmConfigurationPreferences.get_configured_url() + SUFFIX;

            if(authPreferences.getAuthToken() == null){
                Log.d(TAG, "No token provided. Please login first.");
                handler.postDelayed(checkSessionRunning, timerGetRequest);
                return;
            } else {
                handler.removeCallbacks(checkClientLogin);
                NotificationManager notificationManager =
                        (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.cancel("ReminderLogin",404);
            }



            if(esmConfigurationPreferences.getStudyStartMechanics() != 0){
                //stopSelf();
                handler.postDelayed(checkSessionRunning, timerGetRequest);
                return;
            }

            HttpUrl.Builder urlBuilder = HttpUrl.parse(url).newBuilder();
            urlBuilder.addQueryParameter("participant_pseudonym", sessionPreferencesUtil.getParticipantId());

            Request request = new Request.Builder()
                    .header("Authorization", "Token " + authPreferences.getAuthToken())
                    .url(urlBuilder.build())
                    .build();

            okHttpClient.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(@NonNull Call call, @NonNull IOException e) {
                    Log.e(TAG, e.toString());
                    handler.postDelayed(checkSessionRunning, timerGetRequest);
                }

                @Override
                public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                    if(response.isSuccessful()){
                        try{
                            String tmp = response.body().string();
                            JSONObject object = new JSONObject(tmp);
                            Boolean participantStatus = object.getBoolean("participant_status");

                            Log.d(TAG, String.valueOf(participantStatus));

                            if(!isSessionRunning && participantStatus){
                                MessagesSingleton.getInstance().RequestRemotePreSurvey();
                                SessionPreferencesUtil sessionPreferences = new SessionPreferencesUtil(getApplicationContext());
                                sessionPreferences.setSPSessionTimer(System.currentTimeMillis() / 1000);
                            }

                            if(isSessionRunning && ! participantStatus){
                                MessagesSingleton.getInstance().RequestPostSurveys();
                            }

                        } catch (Exception er){
                            Log.e(TAG,er.toString());
                        }

                        handler.postDelayed(checkSessionRunning, timerGetRequest);

                    } else {
                        Log.d(TAG, "Could not fetch participant state");
                        handler.postDelayed(checkSessionRunning, timerGetRequest);
                    }

                }
            });
        }catch (Exception e){
            Log.e(TAG,e.toString());
            handler.postDelayed(checkSessionRunning, timerGetRequest);
            Log.e(TAG,"Repeated call.");


        }
    }

    @Override
    public void onDestroy() {
        //ReceiversManager.getInstance().unregisterSessionReceiver(getApplicationContext());
        stopForeground(false);
        super.onDestroy();
    }
}