package de.dipf.edutec.edutex.androidclient.questionnaire;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import de.dipf.edutec.edutex.androidclient.R;

public class ChoiceAdapter extends RecyclerView.Adapter<ChoiceAdapter.ViewHolder> {

    private List<String> mAnswerChoices;
    private ItemClickListener mItemClickListener;
    private LayoutInflater mInflater;
    private AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.8F);
    private String choosen;
    private Context context;

    //data is passed to the constructor
    public ChoiceAdapter(Context context, List<String> choices){
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.mAnswerChoices = choices;
    }

    public ChoiceAdapter(Context context, List<String> choices, String choosen){
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.mAnswerChoices = choices;
        this.choosen = choosen;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_survey_text, parent, false);



        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        if(choosen != null && String.valueOf(position).equals(choosen)){
            holder.itemView
                    .findViewById(R.id.tv_choice)
                    .setBackground(ContextCompat.getDrawable(context, R.drawable.tv_choice_shape_choosen));
        }


        String choice = mAnswerChoices.get(position);
        holder.mChoiceView.setText(choice);
        if(choice.equals("Voice input") || choice.equals("Keyboard input") || choice.equals("Generate input")){
            if(context != null){
                holder.itemView
                        .findViewById(R.id.tv_choice)
                        .setBackground(ContextCompat.getDrawable(context, R.drawable.tv_choice_shape_user_input));
                }
            }
        //holder.mBtChoiceView.setText(choice);
    }

    @Override
    public int getItemCount() {
        return mAnswerChoices.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnTouchListener {
        public TextView mChoiceView;
        public Button mBtChoiceView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mChoiceView = itemView.findViewById(R.id.tv_choice);
            //mBtChoiceView = itemView.findViewById(R.id.bt_choice);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            v.startAnimation(buttonClick);
            if(mItemClickListener != null) mItemClickListener.onItemClick(itemView, getAdapterPosition());
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            /**
            int x = (int) event.getX();
            int y = (int) event.getY();

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    Log.i("TAG", "touched down");
                    break;
                case MotionEvent.ACTION_MOVE:
                    Log.i("TAG", "moving: (" + x + ", " + y + ")");
                    break;
                case MotionEvent.ACTION_UP:
                    Log.i("TAG", "touched up");
                    break;
            }
            **/
            return true;
        }
    }

    String getItem(int id) {return mAnswerChoices.get(id);}

    public void setClickListener(ItemClickListener itemClickListener){
        this.mItemClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

}
