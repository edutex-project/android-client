package de.dipf.edutec.edutex.androidclient.messageservice;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;

import androidx.annotation.Nullable;

import de.dipf.edutec.edutex.androidclient.conf.CustomApplication;
import de.dipf.edutec.edutex.androidclient.foreground.ForegroundNotificationCreator;
import lombok.SneakyThrows;

public class VibrationService extends Service {

    final static String TAG ="VibrationService";
    ForegroundNotificationCreator fgNotificationCreator;
    Vibrator vibrator;

    @Override
    public void onCreate(){
        super.onCreate();
        this.fgNotificationCreator = ((CustomApplication)getApplication()).getContext().getForegroundNotificationCreator();
    }

    @SneakyThrows
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (vibrator==null){
            vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        } else {
            vibrator.cancel();
        }

        Log.d(TAG, "onStartCommand: called");
        this.fgNotificationCreator = ((CustomApplication)getApplication()).getContext().getForegroundNotificationCreator();
        startForeground(fgNotificationCreator.getId(),fgNotificationCreator.getNotification());
        vibrate_now();
        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy(){
        Log.d(TAG, "onDestroy: called");
        stopForeground(false);
        stopSelf();
    }

    private void vibrate_now(){



        Log.d(TAG, "vibrate_now: called");
        Handler handler = new Handler();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                vibrator.cancel();
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            long[] mVibratePattern = new long[]{0, 400, 800, 600, 800, 800, 800, 1000};
            int[] mAmplitudes = new int[]{0, 255, 0, 255, 0, 255, 0, 255};
            // -1 : Play exactly once
            Log.d(TAG, "vibrate_now: SDK Version >= O");
            if (vibrator.hasAmplitudeControl()) {
                Log.d(TAG, "vibrate_now: Has AmplitudeControl");
                VibrationEffect effect = VibrationEffect.createWaveform(mVibratePattern, mAmplitudes, 3);
                vibrator.vibrate(effect);
                handler.postDelayed(r,6000);

            } else {
                Log.d(TAG, "vibrate_now: Does not have AmplitudeControl");
                long[] mVibratePattern1 = new long[]{0, 400, 800, 600, 800};
                // -1 : Play exactly once
                VibrationEffect effect = VibrationEffect.createWaveform(mVibratePattern1, 3);
                vibrator.vibrate(effect);
                handler.postDelayed(r,6000);

            }
        } else {
            Log.d(TAG, "vibrate_now: SDK Version < O");
            vibrator.vibrate(6000);

        }

        stopForeground(false);
        stopSelf();
    }

}
