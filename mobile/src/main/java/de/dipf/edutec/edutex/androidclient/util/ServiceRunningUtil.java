package de.dipf.edutec.edutex.androidclient.util;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.processtracking.ProcessTrackingService;

import de.dipf.edutec.edutex.androidclient.sensorservice.AmbientNoiseRecordingService;
import de.dipf.edutec.edutex.androidclient.sensorservice.BehavioralRecordingService;
import de.dipf.edutec.edutex.androidclient.sensorservice.BluetoothRecordingService;
import de.dipf.edutec.edutex.androidclient.sensorservice.GPSRecordingService;
import de.dipf.edutec.edutex.androidclient.sensorservice.PhysicalRecordingService;
import de.dipf.edutec.edutex.androidclient.sensorservice.PhysiologicalRecordingService;
import de.dipf.edutec.edutex.androidclient.sensorservice.VirtualSensorService;
import de.dipf.edutec.edutex.androidclient.sensorservice.WifiRecordingService;

public class ServiceRunningUtil {

    public static boolean isMyServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static void terminate_services(Context context){


        // Stopping Recording Services
        if(isMyServiceRunning(PhysiologicalRecordingService.class, context)){
            Intent intent0 = new Intent(context, PhysiologicalRecordingService.class);
            context.stopService(intent0);
        }

        if (isMyServiceRunning(BehavioralRecordingService.class, context)) {
            Intent intent0 = new Intent(context, BehavioralRecordingService.class);
            context.stopService(intent0);
        }

        if (isMyServiceRunning(PhysicalRecordingService.class, context)) {
            Intent intent0 = new Intent(context, PhysicalRecordingService.class);
            context.stopService(intent0);
        }

        if (isMyServiceRunning(AmbientNoiseRecordingService.class, context)) {
            Intent intent0 = new Intent(context, AmbientNoiseRecordingService.class);
            context.stopService(intent0);
        }

        if (isMyServiceRunning(VirtualSensorService.class, context)) {
            Intent intent0 = new Intent(context, VirtualSensorService.class);
            context.stopService(intent0);
        }

        if (isMyServiceRunning(WifiRecordingService.class, context)) {
            Intent intent0 = new Intent(context, WifiRecordingService.class);
            context.stopService(intent0);
        }

        if (isMyServiceRunning(GPSRecordingService.class, context)) {
            Intent intent0 = new Intent(context, GPSRecordingService.class);
            context.stopService(intent0);
        }

        if (isMyServiceRunning(BluetoothRecordingService.class, context)) {
            Intent intent0 = new Intent(context, BluetoothRecordingService.class);
            context.stopService(intent0);
        }


        if(isMyServiceRunning(ProcessTrackingService.class, context)){
            Intent intent0 = new Intent(context, ProcessTrackingService.class);
            context.stopService(intent0);
        }

    }

    public static boolean areServiceClosed(Context context){

        String TAG = "ServiceRunningUtil";

        Class[] classes = new Class[] {
                ProcessTrackingService.class,
                GPSRecordingService.class,
                PhysiologicalRecordingService.class,
                BehavioralRecordingService.class,
                PhysicalRecordingService.class,
                AmbientNoiseRecordingService.class,
                VirtualSensorService.class,
                WifiRecordingService.class,
                BluetoothRecordingService.class,
        };
        boolean response = true;
        for(int i=0; i < classes.length; i++){
            if(isMyServiceRunning(classes[i], context)){
                response = false;
                Intent intent = new Intent(context,  classes[i]);
                context.stopService(intent);
                Log.d(TAG, classes[i].getSimpleName());
                break;
            }
        }
        return response;

    }

}
