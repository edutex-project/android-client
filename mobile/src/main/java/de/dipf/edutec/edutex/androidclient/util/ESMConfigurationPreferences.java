package de.dipf.edutec.edutex.androidclient.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;

public class ESMConfigurationPreferences {




    private static String PREFS_NAME = "ESMConfigurationPreferences";

    private static final String KEY_IS_HTTPS = "is_https";
    private static final String KEY_IS_WSS = "is_wss";
    private static final String KEY_HOST = "used_host";
    private static final String KEY_PORT = "used_port";
    private static final String KEY_IS_DEPLOYED = "is_deployed";
    private static final String KEY_STUDY_IDENTIFIER = "study_identifier";
    public static final String KEY_CONNECTION_STATUS = "connection_status";
    public static final String KEY_CONNECTION_STATUS_WEAR = "connection_mobile";
    public static final String KEY_ERROR_MESSAGE = "error_message";
    public static final String KEY_PREF_DEVICE = "prefered_used_device";
    public static final String KEY_STUDY_MECHANICS = "study_mechanics";
    public static final String KEY_REMINDER_TRACKING_PERMISSIONS = "reminder_tracking_permissions";
    public static final String KEY_ACCEPTED_PRIVACY = "accepted_privacy";
    public static final String KEY_MOODLE_ID = "moodle_id";

    private SharedPreferences sharedPreferences;
    private Context context;

    public ESMConfigurationPreferences(Context context){
        this.context = context;
        this.sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    // Setter Methods
    public void set_is_https(Boolean value){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putBoolean(KEY_IS_HTTPS, value);
        editor.commit();
    }
    public void set_is_wss(Boolean value){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putBoolean(KEY_IS_WSS,value);
        editor.commit();
    }
    public void set_is_https_if_missing(Boolean value){
        if(!this.sharedPreferences.contains(KEY_IS_HTTPS)){
            set_is_https(value);
        }
    }
    public void set_is_wss_if_missing(Boolean value){
        if(!this.sharedPreferences.contains(KEY_IS_WSS)){
            set_is_wss(value);
        }
    }
    public void set_is_deployed(Boolean value){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putBoolean(KEY_IS_DEPLOYED, value);
        editor.commit();
    }
    public void set_host(String host){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putString(KEY_HOST, host);
        editor.commit();
    }
    public void set_port(String port){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putString(KEY_PORT, port);
        editor.commit();
    }
    public void setConntectionStatus(String status){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putString(KEY_CONNECTION_STATUS,status);
        editor.apply();
    }
    public void setConnectionStatusWear(String status){
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putString(KEY_CONNECTION_STATUS_WEAR,status);
        editor.apply();
    }
    public void setStudyIdentifier(String identifier){
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putString(KEY_STUDY_IDENTIFIER,identifier);
        editor.commit();
    }
    public void setErrorMessage(String message){
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putString(KEY_ERROR_MESSAGE,message);
        editor.commit();
    }
    public void setPrefDevice(int deviceType){
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putInt(KEY_PREF_DEVICE, deviceType);
        editor.commit();
    }
    public void setStudyStartMechanics(int mechanics){
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putInt(KEY_STUDY_MECHANICS, mechanics);
        editor.commit();
    }
    public void setAcceptedPrivacy(Boolean isAccepted){
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putBoolean(KEY_ACCEPTED_PRIVACY, isAccepted);
        editor.commit();
    }
    public void setMoodleId(String id){
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putString(KEY_MOODLE_ID, id);
        editor.commit();
    }
    public void setTrackingReminder(Boolean value){
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putBoolean(KEY_REMINDER_TRACKING_PERMISSIONS,value);
        editor.commit();
    }


    // Getter Methods
    public String getConnectionStatus(){return sharedPreferences.getString(KEY_CONNECTION_STATUS,"Disconnected");}
    public String getConnectionStatusWear(){return sharedPreferences.getString(KEY_CONNECTION_STATUS_WEAR,"Disconnected");}
    public Boolean getIsHttps(){return sharedPreferences.getBoolean(KEY_IS_HTTPS,true);}
    public Boolean getIsWss(){return sharedPreferences.getBoolean(KEY_IS_WSS,true);}
    public String getStudyIdentifier(){return sharedPreferences.getString(KEY_STUDY_IDENTIFIER,null);}
    public String getErrorMessage(){
        String error = this.sharedPreferences.getString(KEY_ERROR_MESSAGE,null);
        SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.remove(KEY_ERROR_MESSAGE);
        editor.commit();

        return  error;
    }
    public int getPrefDevice(){
        int deviceType =  this.sharedPreferences.getInt(KEY_PREF_DEVICE,-1);
        if(deviceType == -1){
            SharedPreferences.Editor editor = this.sharedPreferences.edit();
            editor.putInt(KEY_PREF_DEVICE,1);
            editor.commit();
            deviceType = 1;
        }
        return deviceType;
    }
    public Boolean getIsPrivacyAccepted(){
        return this.sharedPreferences.getBoolean(KEY_ACCEPTED_PRIVACY,false);
    }
    public String getHost(){return this.sharedPreferences.getString(KEY_HOST,"esm.edutex.de");}
    public String getPort(){return this.sharedPreferences.getString(KEY_PORT,null);}
    public int getStudyStartMechanics(){
        return this.sharedPreferences.getInt(KEY_STUDY_MECHANICS,0);
    }
    public String getMoodleId(){
        return this.sharedPreferences.getString(KEY_MOODLE_ID, null);
    }

    // Utility Methods
    public String get_configured_url_websocket(){

        String android_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);

        String url = "";
        Boolean iswss = this.sharedPreferences.getBoolean(KEY_IS_WSS,true);
        String host = this.sharedPreferences.getString(KEY_HOST,null);
        String port = this.sharedPreferences.getString(KEY_PORT,null);
        String socket_suffix = "/ws/socket/" + android_id + "/";

        if(port != null && host != null){
            url = host + ":" + port;
        } else {
            url = host;
        }

        if(iswss){
            url = "wss://" + url + socket_suffix;
        } else {
            url = "ws://" + url + socket_suffix;
        }

        return url;

    }
    public String get_configured_url(){

        String url = "";
        Boolean ishttps = this.sharedPreferences.getBoolean(KEY_IS_HTTPS,true);
        String host = this.sharedPreferences.getString(KEY_HOST,null);
        String port = this.sharedPreferences.getString(KEY_PORT,null);

        if(port != null && host != null){
            url = host + ":" + port;
        } else {
            url = host;
        }

        if(ishttps){
            url = "https://" + url;
        } else {
            url = "http://" + url;
        }

        return url;
    }
    public static String getPrefsName(){
        return PREFS_NAME;
    }
    public Boolean isReminderActive(){
        Boolean reminder = this.sharedPreferences.getBoolean(KEY_REMINDER_TRACKING_PERMISSIONS,true);
        return reminder;
    }
    public Boolean isOnlySmartphoneAndRemoteActive(){

        return getStudyStartMechanics() == 0 && getPrefDevice() == 1;

    }
    public Boolean isOnlySmartphoneAndNotRemoteActive(){

        return getStudyStartMechanics() == 1 && getPrefDevice() == 1;
    }

    public Boolean isWithSmartwatchAndRemoteActive(){
        return getStudyStartMechanics() == 0 && getPrefDevice() == 0;
    }

    public Boolean isWithSmartwatchAndNotRemoteActive(){
        return getStudyStartMechanics() == 1 && getPrefDevice() == 0;
    }



}
