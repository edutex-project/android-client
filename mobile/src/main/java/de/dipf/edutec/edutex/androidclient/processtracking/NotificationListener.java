package de.dipf.edutec.edutex.androidclient.processtracking;


import android.os.Build;
import android.os.SystemClock;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.Instant;

import de.dipf.edutec.edutex.androidclient.conf.CustomApplication;
import de.dipf.edutec.edutex.androidclient.foreground.ForegroundNotificationCreator;
import de.dipf.edutec.edutex.androidclient.messageservice.MessagesSingleton;
import de.dipf.edutec.edutex.androidclient.util.TrackingSensorPreferences;
import de.dipf.edutec.edutex.androidclient.sharedUtils.RequestResponseUtils;

public class NotificationListener extends NotificationListenerService {

    private static String TAG = NotificationListener.class.getSimpleName();
    public String ServiceName = "Notification Listener Service";
    public double ServiceVersion = 1.0;


    ForegroundNotificationCreator fgNotificationManager;

    String currentAndroidID;
    TrackingSensorPreferences trackingSensorPreferences;

    @Override
    public void onListenerConnected() {
        super.onListenerConnected();
        Log.d(TAG, "onListenerConnected");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "creating NotificationListener ");
        this.fgNotificationManager =
                ((CustomApplication) getApplication()).getContext().getForegroundNotificationCreator();
        trackingSensorPreferences = new TrackingSensorPreferences(getApplicationContext());
    }



    @Override
    public void onNotificationPosted(StatusBarNotification sbn){
        Instant now = Instant.now();
        long elasped = SystemClock.elapsedRealtimeNanos();
        send_message(sbn, true, now, elasped);
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn){
        Instant now = Instant.now();
        long elasped = SystemClock.elapsedRealtimeNanos();
        send_message(sbn,false, now, elasped);
    }


    public void send_message(StatusBarNotification sbn, Boolean isPosted, Instant now, long elapsed){

        Boolean tracking = trackingSensorPreferences.getTrackNotificationChannel();

        if(tracking){
            JSONObject request = trackingSensorPreferences.getTrackRequest();
            JSONObject original_request = trackingSensorPreferences.getTrackOriginalRequest();
            try {
                JSONObject tmp = new JSONObject();
                tmp.put("service_name",ServiceName);
                tmp.put("service_version",ServiceVersion);
                tmp.put("sensor_name","NotificationListenerService");
                tmp.put("sensor_type", -7);
                tmp.put("datasource","mobile");
                tmp.put("timestamp", RequestResponseUtils.getTimeStamps(now, elapsed));
                tmp.put("values", toJsonObj(isPosted,sbn,now));
                tmp.put("sensor_service_id",request.getInt("id"));
                tmp.put("service_type",request.getInt("service_type"));
                tmp.put("request_id",original_request.getInt("id"));
                tmp.put("request_type", original_request.getString("request_type"));
                tmp.put("event_id",original_request.getString("event_id"));

                JSONArray array2 = new JSONArray();
                array2.put(tmp);

                JSONObject packet = new JSONObject();
                packet.put("events",array2);
                packet.put("request_type","sensor_service");
                packet.put("session_id",original_request.getString("session_id"));

                MessagesSingleton.getInstance().addMessageReceived(packet);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    public JSONObject toJsonObj(boolean posted, StatusBarNotification sbn , Instant now){
        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put("notification_posted",posted);
            jsonObject.put("group_key", sbn.getGroupKey());
            jsonObject.put("id",sbn.getId());
            jsonObject.put("key",sbn.getKey());
            jsonObject.put("notification",sbn.getNotification().toString());
            jsonObject.put("override_group_key",sbn.getOverrideGroupKey());
            jsonObject.put("package_name",sbn.getPackageName());
            jsonObject.put("post_time",sbn.getPostTime());
            jsonObject.put("tag",sbn.getTag());
            jsonObject.put("user", sbn.getUser().toString());
            jsonObject.put("is_clearable",sbn.isClearable());
            jsonObject.put("is_group",sbn.isGroup());
            jsonObject.put("is_ongoing",sbn.isOngoing());
            jsonObject.put("string",sbn.toString());
            //jsonObject.put("date_time", String.valueOf(DateTimeFormatter.ISO_INSTANT.format(now)));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                jsonObject.put("op_pkg",sbn.getOpPkg());
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                jsonObject.put("is_app_group",sbn.isAppGroup());
            }




            Log.d(TAG,jsonObject.toString(4));
        } catch (Exception er){
            Log.d(TAG,"Exception while parsing occured");
        }

        return jsonObject;
    }
    @Override
    public void onDestroy(){
        Log.d(TAG, "onDestroy is called.");
        super.onDestroy();
    }

}
