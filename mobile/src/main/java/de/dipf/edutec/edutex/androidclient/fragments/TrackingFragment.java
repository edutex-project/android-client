package de.dipf.edutec.edutex.androidclient.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import de.dipf.edutec.edutex.androidclient.R;
import de.dipf.edutec.edutex.androidclient.processtracking.WindowChangeDetectingService;
import de.dipf.edutec.edutex.androidclient.util.TrackingPermissionUtil;


public class TrackingFragment extends Fragment {

    private static String TAG = TrackingFragment.class.getSimpleName();
    Button openNotification, openUsage, openWindowDetector;
    TextView txtNotification, txtUsage, txtWindow;

    public TrackingFragment() {
        // Required empty public constructor
    }


    @Override
    public void onResume() {
        super.onResume();
        displayPermissions();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_tracking, container, false);

        openNotification = view.findViewById(R.id.btn_notificationservice);
        openUsage = view.findViewById(R.id.btn_appusagestats);
        openWindowDetector = view.findViewById(R.id.btn_windowchangedetectorservice);

        txtNotification = view.findViewById(R.id.txt_tracking_notification);
        txtUsage = view.findViewById(R.id.txt_tracking_usagestats);
        txtWindow = view.findViewById(R.id.txt_tracking_windowdetection);

        displayPermissions();

        return view;

    }


    private void displayPermissions(){


        Context context = getActivity().getApplicationContext();


        if(TrackingPermissionUtil.isNotificationServiceEnabled(getActivity())){
            txtNotification.setText("enabled");
            txtNotification.setTextColor(ContextCompat.getColor(context, R.color.green_ok));
        } else {
            txtNotification.setText("disabled");
            txtNotification.setTextColor(ContextCompat.getColor(context,R.color.red_ok));
        }

        if (TrackingPermissionUtil.getGrantStatus(getActivity())){
            txtUsage.setText("enabled");
            txtUsage.setTextColor(ContextCompat.getColor(context,R.color.green_ok));
        } else {
            txtUsage.setText("disabled");
            txtUsage.setTextColor(ContextCompat.getColor(context,R.color.red_ok));
        }


        if(TrackingPermissionUtil.isAccessibilityServiceEnabled(getActivity().getApplicationContext(), WindowChangeDetectingService.class)){
            txtWindow.setText("enabled");
            txtWindow.setTextColor(ContextCompat.getColor(context,R.color.green_ok));
        } else {
            txtWindow.setText("disabled");
            txtWindow.setTextColor(ContextCompat.getColor(context,R.color.red_ok));
        }
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        openNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS);
                startActivity(intent);
            }
        });


        openUsage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
                startActivity(intent);
            }
        });

        openWindowDetector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(android.provider.Settings.ACTION_ACCESSIBILITY_SETTINGS);
                startActivity(intent);
            }
        });

    }





}