package de.dipf.edutec.edutex.androidclient.processtracking;

import android.graphics.drawable.Drawable;

import org.json.JSONObject;

import lombok.SneakyThrows;

class AppUsageInfo {
    Drawable appIcon; // You may add get this usage data also, if you wish.
    String appName, packageName;
    long timeInForeground;
    int launchCount;

    AppUsageInfo(String pName) {
        this.packageName=pName;
    }

    @SneakyThrows
    public JSONObject reformat(){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("app_name",appName);
        jsonObject.put("package_name",packageName);
        jsonObject.put("time_in_foreground",timeInForeground);
        jsonObject.put("launch_count",launchCount);
        return jsonObject;
    }

}